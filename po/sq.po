# Përkthimi i mesazheve të firestarter në shqip
# This file is distributed under the same license as the firestarter package.
# Laurent Dhima <laurenti@alblinux.net>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: firestarter HEAD\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2004-09-02 11:15+0200\n"
"PO-Revision-Date: 2004-09-03 17:52+0200\n"
"Last-Translator: Laurent Dhima <laurenti@alblinux.net>\n"
"Language-Team: Albanian <gnome-albanian-perkthyesit@lists.sourceforge.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: firestarter.desktop.in.h:1
msgid "Desktop Firewall Tool"
msgstr "Instrumenti i Desktop për Firewall"

#: firestarter.desktop.in.h:2
msgid "Firestarter"
msgstr "Firestarter"

#: firestarter.schemas.in.h:1
#, fuzzy
msgid ""
"Allow incoming DHCP packets, and start the firewall when an IP lease is "
"obtained."
msgstr "Lejo në ardhje dhe nisje IP është."

#: firestarter.schemas.in.h:2
#, fuzzy
msgid ""
"Always convert the entries in the Hit View from IP format to a valid "
"hostname. This options can slow down the client a lot, as it is not threaded."
msgstr "Gjithmonë konvertoje në Luaj Paraqitja IP a emri i host poshtë a është nuk."

#: firestarter.schemas.in.h:3
#, fuzzy
msgid ""
"Apply changes made to policy immediately, the alternative is for the user to "
"confirm the changes in advance."
msgstr "Apliko menjëherë është për përdorues konfermo në."

#: firestarter.schemas.in.h:4
msgid "Apply the ICMP settings, as configured in the wizard."
msgstr "Apliko rregullimet ICMP, sikurse konfiguruar gjatë wizard."

#: firestarter.schemas.in.h:5
msgid "Apply the settings of Type of Service, as configured in the wizard."
msgstr "Apliko rregullimet e Llojit të Shërbimit, sikurse konfiguruar gjatë wizard."

#: firestarter.schemas.in.h:6
msgid "Convert IPs to human readable form"
msgstr "Konverto adresat IP në një formë të kuptueshme nga njerzit"

#: firestarter.schemas.in.h:7
msgid "Deny packets"
msgstr "Blloko paketat"

#: firestarter.schemas.in.h:8
#, fuzzy
msgid "Drop denied packets without sending an error packet back."
msgstr "Hidh gabim mbrapa."

#: firestarter.schemas.in.h:9
#, fuzzy
msgid "Enable DHCP server for connection sharing"
msgstr "Aktivo Network Address Translation, ose ndarjen e lidhjes Internet."

#: firestarter.schemas.in.h:10
msgid "Enable NAT"
msgstr "Aktivo NAT"

#: firestarter.schemas.in.h:11
msgid "Enable Network Address Translation, or Internet connection sharing."
msgstr "Aktivo Network Address Translation, ose ndarjen e lidhjes Internet."

#: firestarter.schemas.in.h:12
msgid "Enable the configuration of ICMP"
msgstr "Aktivo konfigurimin e ICMP"

#: firestarter.schemas.in.h:13
msgid "Enable the configuration of tos"
msgstr "Aktivo konfigurimin e tos"

#: firestarter.schemas.in.h:14
msgid "External interface is modem"
msgstr "Interfaqja e jashtme është modemi"

#: firestarter.schemas.in.h:15
msgid "External interface uses DHCP"
msgstr "Interfaqja e jashtme përdor DHCP"

#: firestarter.schemas.in.h:16 src/preferences.c:279
#, fuzzy
msgid "Filter out hits not meant for your machine"
msgstr "Filtri hits nuk për"

#: firestarter.schemas.in.h:17
#, fuzzy
msgid "Filter out hits that have an destination IP that does not match your IP."
msgstr "Filtri hits IP nuk IP."

#: firestarter.schemas.in.h:18 src/preferences.c:271
#, fuzzy
msgid "Filter out redundant hits"
msgstr "Filtri hits"

#: firestarter.schemas.in.h:19
#, fuzzy
msgid "Filter out sequences of identical hits."
msgstr "Filtri nga hits."

#: firestarter.schemas.in.h:20
#, fuzzy
msgid "Highest IP in the range to serve to DHCP clients"
msgstr "IP në"

#: firestarter.schemas.in.h:21
#, fuzzy
msgid "Is it the first time the user is running the program"
msgstr "i pari ora përdorues është në punim e sipër programi"

#: firestarter.schemas.in.h:22
#, fuzzy
msgid "Lowest IP in the range to serve to DHCP clients"
msgstr "IP në"

#: firestarter.schemas.in.h:23
msgid "Nameserver DHCP clients should use"
msgstr ""

#: firestarter.schemas.in.h:24
#, fuzzy
msgid "Policy auto apply"
msgstr "apliko"

#: firestarter.schemas.in.h:25
#, fuzzy
msgid ""
"Restrict outbound traffic by default, only allowing whitelisted outgoing "
"connections."
msgstr "nga prezgjedhur."

#: firestarter.schemas.in.h:26
#, fuzzy
msgid "Restrictive outbound policy mode"
msgstr "modaliteti"

#: firestarter.schemas.in.h:27
#, fuzzy
msgid ""
"Run the system dhcpd server in conjunction with Firestarter Internet "
"connection sharing."
msgstr "Ekzekuto sistemi server në me Firestarter Internet."

#: firestarter.schemas.in.h:28
#, fuzzy
msgid "Show the Type of Service column in the Hit View."
msgstr "Shfaq Lloji nga Shërbimi në Luaj Paraqitja."

#: firestarter.schemas.in.h:29
#, fuzzy
msgid "Show the destination column in the Hit View"
msgstr "Shfaq në Luaj Paraqitja"

#: firestarter.schemas.in.h:30
#, fuzzy
msgid "Show the destination column in the Hit View."
msgstr "Shfaq në Luaj Paraqitja."

#: firestarter.schemas.in.h:31
#, fuzzy
msgid "Show the device the hit was recieved from in the Hit View."
msgstr "Shfaq periferik në Luaj Paraqitja."

#: firestarter.schemas.in.h:32
#, fuzzy
msgid "Show the device the hit was routed to in the Hit View."
msgstr "Shfaq periferik në Luaj Paraqitja."

#: firestarter.schemas.in.h:33
#, fuzzy
msgid "Show the direction column in the Hit View"
msgstr "Shfaq në Luaj Paraqitja"

#: firestarter.schemas.in.h:34
#, fuzzy
msgid "Show the direction column in the Hit View."
msgstr "Shfaq në Luaj Paraqitja."

#: firestarter.schemas.in.h:35
#, fuzzy
msgid "Show the in column in the Hit View"
msgstr "Shfaq në në Luaj Paraqitja"

#: firestarter.schemas.in.h:36
#, fuzzy
msgid "Show the length column in the Hit View"
msgstr "Shfaq në Luaj Paraqitja"

#: firestarter.schemas.in.h:37
#, fuzzy
msgid "Show the out column in the Hit View"
msgstr "Shfaq në Luaj Paraqitja"

#: firestarter.schemas.in.h:38
#, fuzzy
msgid "Show the packet length column in the Hit View."
msgstr "Shfaq në Luaj Paraqitja."

#: firestarter.schemas.in.h:39
#, fuzzy
msgid "Show the port column in the Hit View"
msgstr "Shfaq në Luaj Paraqitja"

#: firestarter.schemas.in.h:40
#, fuzzy
msgid "Show the port column in the Hit View."
msgstr "Shfaq në Luaj Paraqitja."

#: firestarter.schemas.in.h:41
#, fuzzy
msgid "Show the protocol column in the Hit View"
msgstr "Shfaq në Luaj Paraqitja"

#: firestarter.schemas.in.h:42
#, fuzzy
msgid "Show the protocol column in the Hit View."
msgstr "Shfaq në Luaj Paraqitja."

#: firestarter.schemas.in.h:43
#, fuzzy
msgid "Show the service column in the Hit View"
msgstr "Shfaq në Luaj Paraqitja"

#: firestarter.schemas.in.h:44
#, fuzzy
msgid "Show the service column in the Hit View."
msgstr "Shfaq në Luaj Paraqitja."

#: firestarter.schemas.in.h:45
#, fuzzy
msgid "Show the source column in the Hit View"
msgstr "Shfaq në Luaj Paraqitja"

#: firestarter.schemas.in.h:46
#, fuzzy
msgid "Show the source column in the Hit View."
msgstr "Shfaq në Luaj Paraqitja."

#: firestarter.schemas.in.h:47
#, fuzzy
msgid "Show the time column in the Hit View"
msgstr "Shfaq ora në Luaj Paraqitja"

#: firestarter.schemas.in.h:48
#, fuzzy
msgid "Show the time column in the Hit View."
msgstr "Shfaq ora në Luaj Paraqitja."

#: firestarter.schemas.in.h:49
#, fuzzy
msgid "Show the tos column in the Hit View"
msgstr "Shfaq në Luaj Paraqitja"

#: firestarter.schemas.in.h:50
#, fuzzy
msgid "Specifies the higher limit of the IP space to use for DHCP leases."
msgstr "nga IP hapësirë për."

#: firestarter.schemas.in.h:51
#, fuzzy
msgid "Specifies the lower limit of the IP space to use for DHCP leases."
msgstr "nga IP hapësirë për."

#: firestarter.schemas.in.h:52
#, fuzzy
msgid "Start the firewall when the program loads"
msgstr "Fillo programi"

#: firestarter.schemas.in.h:53
#, fuzzy
msgid "Start the firewall when the program loads."
msgstr "Fillo programi."

#: firestarter.schemas.in.h:54
#, fuzzy
msgid "Start the firewall when the user dials out. Modifies /etc/ppp/ip-up.local."
msgstr "Fillo përdorues sipër lokal."

#: firestarter.schemas.in.h:55
#, fuzzy
msgid "Stop the firewall when the program exists"
msgstr "Ndal programi ekziston"

#: firestarter.schemas.in.h:56
#, fuzzy
msgid "Stop the firewall when the program exists."
msgstr "Ndal programi ekziston."

#: firestarter.schemas.in.h:57 src/preferences.c:229
#, fuzzy
msgid "System tray operation mode"
msgstr "Sistemi modaliteti"

#: firestarter.schemas.in.h:58
msgid "The Internet connected network interface. For example, ppp0 or eth0."
msgstr "Interfaqja e rrjetit për lidhjen Internet. P.sh., ppp0 ose eth0."

#: firestarter.schemas.in.h:59
msgid "The LAN connected network interface. For example, eth0 or eth1."
msgstr "Interfaqja e rrjetit e lidhur me LAN. P.sh., eth0 ose eth1."

#: firestarter.schemas.in.h:60
msgid "The external interface"
msgstr "Interfaqja e jashtme"

#: firestarter.schemas.in.h:61
#, fuzzy
msgid "The first time Firestarter starts several special actions are taken."
msgstr "i pari ora Firestarter."

#: firestarter.schemas.in.h:62
msgid "The internal interface"
msgstr "Interfaqja e brendshme"

#: firestarter.schemas.in.h:63
#, fuzzy
msgid "The location of the file the system logging daemon writes to."
msgstr "pozicioni nga sistemi."

#: firestarter.schemas.in.h:64
#, fuzzy
msgid ""
"The nameserver clients should use. You can specify multiple nameserver in a "
"space separated list. A value of &lt;dynamic&gt; causes the nameservers to "
"be determined at run time."
msgstr "Ti në a hapësirë lista A nga&lt;&gt; ora."

#: firestarter.schemas.in.h:65
msgid "The system log file"
msgstr "File log i sistemit"

#: firestarter.schemas.in.h:66
#, fuzzy
msgid ""
"When closing the program window in tray mode the program is hid instead of "
"exited. Works together with the notification area applet."
msgstr "Kur programi dritare në modaliteti programi është nga me."

#: src/dhcp-server.c:108
#, fuzzy
msgid "Failed to open DHCP server configuration file for writing: "
msgstr "Dështoi hap server për "

#: src/dhcp-server.c:115
#, fuzzy
msgid "Failed to write to DHCP server configuration file: "
msgstr "Dështoi server "

#: src/firestarter.c:75
#, c-format
msgid "Firewall stopped, network traffic is now flowing freely\n"
msgstr "Firewall u ndalua, trafiku i rrjetit tashmë rrjedh pa pengesa\n"

#: src/firestarter.c:82
msgid ""
"Failed to stop the firewall\n"
"\n"
msgstr ""
"Ndalimi i firewall dështoi\n"
"\n"

#: src/firestarter.c:87
msgid "There was an undetermined error when trying to stop your firewall."
msgstr "Gabim i papërcaktuar gjatë përpjekjes për të ndaluar firewall-in tuaj."

#: src/firestarter.c:118
msgid "Firewall started\n"
msgstr "Firewwal u nis\n"

#: src/firestarter.c:126
#, fuzzy
msgid ""
"Failed to start the firewall:\n"
"\n"
msgstr ""
"Nisja e firewall dështoi\n"
"\n"

#: src/firestarter.c:132 src/firestarter.c:136
#, c-format
msgid ""
"The device %s is not ready.\n"
"\n"
msgstr ""
"Periferikja %s nuk është gati.\n"
"\n"

#: src/firestarter.c:140
#, fuzzy
msgid ""
"Your kernel does not support iptables.\n"
"\n"
msgstr "nuk j j"

#: src/firestarter.c:144
#, fuzzy
msgid ""
"An unknown error occurred.\n"
"\n"
msgstr "nuk njihet gabim j j"

#: src/firestarter.c:149
msgid ""
"Please check your network device settings and make sure your\n"
"Internet connection is active."
msgstr ""
"Ju lutem kontrolloni rregullimet e periferikes suaj të rrjetit dhe "
"sigurohuni\n"
"që lidhja me Internet është aktive."

#: src/firestarter.c:185
#, fuzzy
msgid "All traffic blocked\n"
msgstr "I gjithë trafiku u bllokua\n"

#: src/firestarter.c:193
#, fuzzy
msgid ""
"Failed to lock down firewall\n"
"\n"
msgstr ""
"Ndalimi i firewall dështoi\n"
"\n"

#: src/firestarter.c:198
#, fuzzy
msgid "There was an undetermined error when trying to lock the firewall."
msgstr "Gabim i papërcaktuar gjatë përpjekjes për të ndaluar firewall-in tuaj."

#: src/firestarter.c:270 src/tray.c:99
msgid "Firewall stopped"
msgstr "Firewall u ndalua"

#: src/firestarter.c:273 src/tray.c:102
msgid "Firewall running"
msgstr "Firewall në ekzekutim"

#: src/firestarter.c:276 src/tray.c:105
#, fuzzy
msgid "Firewall locked"
msgstr "Firewall u ndalua"

#: src/firestarter.c:312
msgid "Firestarter "
msgstr "Firestarter "

#: src/firestarter.c:312
#, fuzzy
msgid ""
"\n"
"\n"
" -s, --start            Start the firewall\n"
" -p, --stop             Stop the firewall\n"
"     --lock             Lock the firewall, blocking all outgoing and "
"incoming traffic\n"
"     --generate-scripts Generate firewall scripts from current "
"configuration\n"
"     --start-hidden     Start Firestarter with the GUI not visible\n"
" -v, --version          Prints Firestarter's version number\n"
" -h, --help             You're looking at it\n"
msgstr ""
"\n"
"\n"
" -s, --start            Nis firewall\n"
" -p, --stop             Ndalon firewall\n"
"     --halt             Bllokon të gjithë trafikun në dalje dhe në hyrje "
"(panic mode)\n"
"     --generate-scripts Gjeneron skripte firewall nga konfigurimi aktual "
"configuration\n"
" -v, --version          Printon numrin e versionit të Firestarter\n"
" -h, --help             Jeni duke e shikuar\n"

#: src/firestarter.c:331
msgid ""
"Insufficient privileges\n"
"\n"
msgstr ""
"Të drejta të pamjaftueshme\n"
"\n"

#: src/firestarter.c:334
msgid "You must have root user privileges to use Firestarter.\n"
msgstr "Duhet të keni të drejtat e përdoruesit root që të përdorni Firestarter.\n"

#: src/gui.c:155
msgid "An all-in-one Linux firewall utility for GNOME.\n"
msgstr "Program Linux firewall 'E gjitha në një' për GNOME.\n"

#: src/gui.c:264 src/statusview.c:656
msgid "Status"
msgstr "Gjendja"

#: src/gui.c:269 src/statusview.c:662
msgid "Events"
msgstr "Ndodhitë"

#: src/gui.c:274
msgid "Policy"
msgstr ""

#. column for time
#: src/hitview.c:429
msgid "Time"
msgstr "Ora"

#. column for direction
#: src/hitview.c:436
#, fuzzy
msgid "Direction"
msgstr "Destinimi"

#. column for in device
#: src/hitview.c:443
msgid "In"
msgstr "Zmadho"

#. column for out device
#: src/hitview.c:450
msgid "Out"
msgstr "Zvogëlo"

#. column for port
#: src/hitview.c:457 src/policyview.c:498 src/policyview.c:1005
#: src/policyview.c:1012 src/policyview.c:1030 src/policyview.c:1048
#: src/statusview.c:632
msgid "Port"
msgstr "Porta"

#. column for source
#: src/hitview.c:464 src/statusview.c:630
msgid "Source"
msgstr "Burimi"

#. column for destination
#: src/hitview.c:471 src/statusview.c:631
msgid "Destination"
msgstr "Destinimi"

#. column for packet length
#: src/hitview.c:478
msgid "Length"
msgstr "Gjatësia"

#. column for ToS
#: src/hitview.c:485
msgid "TOS"
msgstr "TOS"

#. column for protocol
#: src/hitview.c:492
msgid "Protocol"
msgstr "Protokolli"

#. column for service
#: src/hitview.c:499 src/statusview.c:633
msgid "Service"
msgstr "Shërbimi"

#: src/hitview.c:952
#, fuzzy
msgid "Blocked Connections"
msgstr "Bllokuar Lidhjet"

#: src/netfilter-script.c:730
#, c-format
#, fuzzy
msgid "Firewall script saved as %s\n"
msgstr "Firewall j"

#: src/policyview.c:436 src/policyview.c:530
#, fuzzy
msgid "IP, host or network"
msgstr "IP"

#: src/policyview.c:483
#, fuzzy
msgid "Name"
msgstr "Emri"

#: src/policyview.c:524
msgid "Anyone"
msgstr ""

#: src/policyview.c:526
#, fuzzy
msgid "Firewall host"
msgstr "Firewall"

#: src/policyview.c:528
msgid "LAN clients"
msgstr ""

#: src/policyview.c:844 src/policyview.c:878
#, fuzzy
msgid "Failed to apply the policy"
msgstr ""
"Ndalimi i firewall dështoi\n"
"\n"

#: src/policyview.c:845 src/policyview.c:879
#, fuzzy
msgid "There was an error when trying to apply the policy: "
msgstr "Gabim i papërcaktuar gjatë përpjekjes për të ndaluar firewall-in tuaj."

#: src/policyview.c:999
#, fuzzy
msgid "Allow connections from host"
msgstr "Lejo"

#: src/policyview.c:1000 src/policyview.c:1007 src/policyview.c:1014
#: src/policyview.c:1020 src/policyview.c:1025 src/policyview.c:1032
#: src/policyview.c:1038 src/policyview.c:1043 src/policyview.c:1050
#: src/policyview.c:1076 src/policyview.c:1094 src/policyview.c:1155
#: src/policyview.c:1172 src/policyview.c:1190 src/policyview.c:1210
#: src/policyview.c:1227 src/policyview.c:1245
#, fuzzy
msgid "Comment"
msgstr "Komenti"

#: src/policyview.c:1004 src/policyview.c:1047 src/policyview.c:1092
#: src/policyview.c:1243
#, fuzzy
msgid "Allow service"
msgstr "Shërbimi"

#: src/policyview.c:1006 src/policyview.c:1031 src/policyview.c:1049
msgid "For"
msgstr ""

#: src/policyview.c:1011
#, fuzzy
msgid "Forward service"
msgstr "Vazhdo"

#: src/policyview.c:1013
#, fuzzy
msgid "To"
msgstr "Për"

#: src/policyview.c:1019
msgid "Deny connections to host"
msgstr ""

#: src/policyview.c:1024
msgid "Deny connections from LAN host"
msgstr ""

#: src/policyview.c:1029 src/policyview.c:1188
#, fuzzy
msgid "Deny service"
msgstr "Dispozitivi"

#: src/policyview.c:1037
#, fuzzy
msgid "Allow connections to host"
msgstr "Lejo"

#: src/policyview.c:1042
#, fuzzy
msgid "Allow connections from LAN host"
msgstr "Lejo"

#: src/policyview.c:1062
msgid "_Inbound traffic"
msgstr ""

#: src/policyview.c:1074 src/policyview.c:1091
#, fuzzy
msgid "Add new inbound rule"
msgstr "Shto i ri"

#: src/policyview.c:1075 src/policyview.c:1226
#, fuzzy
msgid "Allow connections from"
msgstr "Lejo"

#: src/policyview.c:1093 src/policyview.c:1189 src/policyview.c:1244
#, fuzzy
msgid "When the source is"
msgstr "Kur është"

#: src/policyview.c:1118
msgid "_Outbound traffic"
msgstr ""

#: src/policyview.c:1132
#, fuzzy
msgid "Permissive by default, blacklist traffic"
msgstr "nga prezgjedhur"

#: src/policyview.c:1138
#, fuzzy
msgid "Restrictive by default, whitelist traffic"
msgstr "nga prezgjedhur"

#: src/policyview.c:1153 src/policyview.c:1170 src/policyview.c:1187
#: src/policyview.c:1208 src/policyview.c:1225 src/policyview.c:1242
#, fuzzy
msgid "Add new outbound rule"
msgstr "Shto i ri"

#: src/policyview.c:1154
msgid "Deny connections to"
msgstr ""

#: src/policyview.c:1171
msgid "Deny connections from"
msgstr ""

#: src/policyview.c:1209
#, fuzzy
msgid "Allow connections to"
msgstr "Lejo"

#: src/policyview.c:1263
#, fuzzy
msgid "Auto apply changes"
msgstr "Automatike apliko"

#: src/preferences.c:72
#, fuzzy
msgid ""
"A proper configuration for Firestarter was not found. If you are running "
"Firestarter from the directory you built it in, run 'make install-data-"
"local' to install a configuration, or simply 'make install' to install the "
"whole program.\n"
"\n"
"Firestarter will now close."
msgstr "A për Firestarter nuk në punim e sipër Firestarter në instalo lokal instalo a instalo instalo programi j tani mbylle."

#: src/preferences.c:201
#, fuzzy
msgid ""
"The general program settings let you configure the overall behavior\n"
"of the program.\n"
msgstr "programi rregullimet konfiguro programi j"

#: src/preferences.c:206
#, fuzzy
msgid "Start firewall on program startup"
msgstr "Fillo në programi"

#: src/preferences.c:208
#, fuzzy
msgid "Check this and the firewall will be started when the program loads."
msgstr "Kontrolli dhe programi."

#: src/preferences.c:215
#, fuzzy
msgid "Stop firewall on program exit"
msgstr "Ndal në programi"

#: src/preferences.c:217
#, fuzzy
msgid "Check this and the firewall will be stopped when you exit the program."
msgstr "Kontrolli dhe i ndalur programi."

#: src/preferences.c:222
#, fuzzy
msgid "Autoresolve IP numbers to hostnames for hits"
msgstr "IP emrat e host për hits"

#: src/preferences.c:224
#, fuzzy
msgid ""
"Check this and instead of the IP number you will see a hostname in the Hits "
"view. This option uses some network bandwidth."
msgstr "Kontrolli dhe nga IP a emri i host në shfaq."

#: src/preferences.c:231
#, fuzzy
msgid ""
"When closing the program window in tray mode the program is hid instead of "
"exited and can be restored by clicking the Firestarter system tray icon."
msgstr "Kur programi dritare në modaliteti programi është nga dhe nga Firestarter sistemi ikonë."

#: src/preferences.c:266
#, fuzzy
msgid ""
"The filtering settings let you weed out excessive information\n"
"from the Hits view.\n"
msgstr "rregullimet shfaq j"

#: src/preferences.c:273
#, fuzzy
msgid ""
"Check this and the Hits view will not show identical hits that follow each "
"other."
msgstr "Kontrolli dhe shfaq nuk hits ndjek tjetër."

#: src/preferences.c:281
#, fuzzy
msgid ""
"Check this and the hits list will not show hits that have a destination "
"field other than your IP."
msgstr "Kontrolli dhe hits lista nuk hits a tjetër IP."

#: src/preferences.c:316
#, fuzzy
msgid ""
"The advanced settings let the experienced user fine tune\n"
"the program operation.\n"
msgstr "rregullimet përdorues programi j"

#: src/preferences.c:320
#, fuzzy
msgid "Enable Experimental Options"
msgstr "Aktivo Mundësitë"

#: src/preferences.c:322
#, fuzzy
msgid ""
"If you enable this, Firestarter can use some experimental patches for "
"filtering. Do not enable this option until you have read the manual for the "
"prerequisites."
msgstr "aktiv Firestarter për Bëje nuk aktiv deri më manual për."

#: src/preferences.c:326
msgid "Preferred packet rejection method"
msgstr ""

#: src/preferences.c:333
msgid "Deny"
msgstr ""

#: src/preferences.c:335
msgid "Silently ignore the rejected packets."
msgstr ""

#: src/preferences.c:340
msgid "Reject"
msgstr "Refuzim"

#: src/preferences.c:344
#, fuzzy
msgid "Reply with a notice of rejection to the sender."
msgstr "Përgjigju me a nga."

#: src/preferences.c:409
#, fuzzy
msgid "Firestarter: Preferences"
msgstr "Firestarter Preferimet"

#: src/preferences.c:421
msgid "General"
msgstr "Të përgjithshme"

#: src/preferences.c:422
msgid "Filters"
msgstr "Filtrat"

#: src/preferences.c:445
#, fuzzy
msgid "External Device"
msgstr "Dispozitivi"

#: src/preferences.c:446
#, fuzzy
msgid "Connection Sharing"
msgstr "Lidhja Duke ndarë"

#: src/preferences.c:447
msgid "ICMP Filtering"
msgstr ""

#: src/preferences.c:448
#, fuzzy
msgid "Type of Service"
msgstr "Lloji nga Shërbimi"

#: src/preferences.c:480
msgid "Advanced"
msgstr "Të detajuara"

#: src/savelog.c:67
#, c-format
#, fuzzy
msgid ""
"Error writing to file %s\n"
"\n"
"%s"
msgstr "Gabim j j"

#: src/savelog.c:92
#, fuzzy
msgid "Save Events To File"
msgstr "Ruaj Ndodhitë Për File"

#: src/scriptwriter.c:592
#, fuzzy
msgid ""
"No DHCP client configuration found\n"
"\n"
"The firewall will not be loaded automatically on a lease renewal. Please "
"make sure the external device is configured properly or disable the DHCP "
"option if you are using static settings.\n"
msgstr "Jo j nuk në a i jashtëm periferik është ç'aktivo rregullimet j"

#: src/statusview.c:230 src/statusview.c:728
msgid "Active"
msgstr "Aktiv"

#: src/statusview.c:232
#, fuzzy
msgid "Disabled"
msgstr "Jo aktiv"

#: src/statusview.c:234
#, fuzzy
msgid "Locked"
msgstr "E bllokuar"

#: src/statusview.c:417
msgid "Internet"
msgstr "Internet"

#: src/statusview.c:420
msgid "Local"
msgstr "Lokale"

#: src/statusview.c:634
#, fuzzy
msgid "Color"
msgstr "Pastro"

#: src/statusview.c:644
msgid "Firewall"
msgstr "Firewall"

#: src/statusview.c:674
msgid "Total"
msgstr "Gjithsej"

#: src/statusview.c:680
msgid "Serious"
msgstr ""

#: src/statusview.c:686
msgid "Inbound"
msgstr ""

#: src/statusview.c:693
msgid "Outbound"
msgstr ""

#: src/statusview.c:740
msgid "Network"
msgstr "Rrjeti"

#: src/statusview.c:752
msgid "Device"
msgstr "Dispozitivi"

#: src/statusview.c:758
msgid "Type"
msgstr "Lloji"

#: src/statusview.c:764
#, fuzzy
msgid "Received"
msgstr "Marrë"

#: src/statusview.c:770
msgid "Sent"
msgstr "Dërguar"

#: src/statusview.c:776
msgid "Activity"
msgstr "Aktiviteti"

#: src/statusview.c:783
#, fuzzy
msgid "Active connections"
msgstr "Aktiv"

#: src/tray.c:155
#, fuzzy
msgid "Start firewall"
msgstr "Fillo"

#: src/tray.c:164
#, fuzzy
msgid "Stop firewall"
msgstr "Ndal"

#: src/tray.c:177
msgid "Exit"
msgstr "Dalja"

#: src/util.c:55
#, fuzzy
msgid "Firestarter error"
msgstr "Firestarter gabim"

#: src/util.c:127
#, fuzzy
msgid ""
"Failed to open the system log\n"
"\n"
msgstr "Dështoi hap sistemi j j"

#: src/util.c:129
#, fuzzy
msgid "No realtime hit information will be available."
msgstr "Jo."

#: src/util.c:224
#, fuzzy
msgid "Failed to open /proc/net/dev for reading: "
msgstr "Dështoi hap për "

#: src/util.c:267
#, fuzzy
msgid "Ethernet device"
msgstr "Ethernet periferik"

#: src/util.c:269
#, fuzzy
msgid "Ethernet"
msgstr "Internet"

#: src/util.c:272
#, fuzzy
msgid "Dialup device"
msgstr "Dispozitivi"

#: src/util.c:274
msgid "Dialup"
msgstr ""

#: src/util.c:277
#, fuzzy
msgid "Wireless device"
msgstr "Wireless (pa fije) periferik"

#: src/util.c:279
#, fuzzy
msgid "Wireless"
msgstr "Wireless (pa fije)"

#: src/util.c:281
msgid "IPv6 Tunnel"
msgstr ""

#: src/util.c:283
msgid "VPN Tunnel"
msgstr ""

#: src/util.c:285
#, fuzzy
msgid "Routed IP Tunnel"
msgstr "IP"

#: src/util.c:288
#, fuzzy
msgid "Unknown device"
msgstr "Nuk njihet"

#: src/util.c:290
msgid "Unknown"
msgstr "Nuk njihet"

#: src/wizard.c:114
#, fuzzy
msgid ""
"Please select your Internet connected network device from the drop-down\n"
"list of available devices."
msgstr "zgjidh Internet periferik poshtë nga."

#: src/wizard.c:125
#, fuzzy
msgid "Detected device(s):"
msgstr "periferik:"

#: src/wizard.c:145
#, fuzzy
msgid ""
"Tip: If you use a modem the device name is likely ppp0. If you have a cable "
"modem or a\n"
"DSL connection, choose eth0. Choose ppp0 if you know your cable or DSL "
"operator uses\n"
"the PPPoE protocol."
msgstr "Tip a periferik emri është a a."

#: src/wizard.c:155
#, fuzzy
msgid "Start the firewall on dial-out"
msgstr "Fillo në telefono"

#: src/wizard.c:157
#, fuzzy
msgid ""
"Check this option and the firewall will start when you dial your Internet "
"Service Provider."
msgstr "Kontrolli dhe nisje telefono Internet Shërbimi."

#: src/wizard.c:163
#, fuzzy
msgid "IP address is assigned via DHCP"
msgstr "IP është"

#: src/wizard.c:165
#, fuzzy
msgid ""
"Check this option if you need to connect to a DHCP server. Cable modem and "
"DSL users should check this."
msgstr "Kontrolli a server dhe përdorues."

#: src/wizard.c:245
#, fuzzy
msgid ""
"Firestarter can share your Internet connection with the computers on your "
"local network\n"
"using a single public IP address and a method called Network Address "
"Translation."
msgstr "Firestarter Internet me në lokal a IP dhe a Rrjeti Adresa Përkthimi."

#: src/wizard.c:253
#, fuzzy
msgid "Enable Internet connection sharing"
msgstr "Aktivo Network Address Translation, ose ndarjen e lidhjes Internet."

#: src/wizard.c:271
#, fuzzy
msgid "Local area network device:"
msgstr "Lokale periferik:"

#: src/wizard.c:291
#, fuzzy
msgid "Enable DHCP for local network"
msgstr "Aktivo për lokal"

#: src/wizard.c:295
msgid "Explain the DHCP function..."
msgstr ""

#: src/wizard.c:301
#, fuzzy
msgid "DHCP server details"
msgstr "server detajet"

#: src/wizard.c:321
msgid "Keep existing DHCP configuration"
msgstr ""

#: src/wizard.c:325
#, fuzzy
msgid "Create new DHCP configuration:"
msgstr "Krijo i ri:"

#: src/wizard.c:400
#, fuzzy
msgid ""
"ICMP packet filtering can be useful to prevent some common\n"
"Denial of Service (DoS) attacks on your network."
msgstr "nga Shërbimi në."

#: src/wizard.c:408
#, fuzzy
msgid "Disable ICMP filtering"
msgstr "Ç'aktivo"

#: src/wizard.c:413
#, fuzzy
msgid "Enable ICMP filtering related to the following packets:"
msgstr "Aktivo:"

#: src/wizard.c:429
msgid "Echo"
msgstr "Echo"

#: src/wizard.c:432
msgid "Traceroute"
msgstr "Gjurmat e kalimit"

#: src/wizard.c:435
#, fuzzy
msgid "MS Traceroute"
msgstr "Gjurmat e kalimit"

#: src/wizard.c:438
msgid "Unreachable"
msgstr ""

#: src/wizard.c:446
msgid "Timestamping"
msgstr ""

#: src/wizard.c:449
#, fuzzy
msgid "Address Masking"
msgstr "Adresa"

#: src/wizard.c:452
msgid "Redirection"
msgstr ""

#: src/wizard.c:455
#, fuzzy
msgid "Source Quenches"
msgstr "Burimi"

#: src/wizard.c:481
#, fuzzy
msgid ""
"Type of Service filtering allows you to re-prioritize network services\n"
"to allow higher throughput rates for commonly used services."
msgstr "Lloji nga Shërbimi për."

#: src/wizard.c:489
#, fuzzy
msgid "Disable ToS filtering"
msgstr "Ç'aktivo"

#: src/wizard.c:494
#, fuzzy
msgid "Enable ToS filtering related to the following packets:"
msgstr "Aktivo:"

#: src/wizard.c:510
#, fuzzy
msgid "Client Applications"
msgstr "Programe"

#: src/wizard.c:514
#, fuzzy
msgid ""
"Check this option to enable Type of Service flags for common client "
"applications, such as WWW, FTP, SSH & E-Mail."
msgstr "Kontrolli aktiv Lloji nga Shërbimi për WWW FTP SSH E Posta."

#: src/wizard.c:517
#, fuzzy
msgid "Server Applications"
msgstr "Serveri Programe"

#: src/wizard.c:521
#, fuzzy
msgid ""
"Check this option to enable Type of Service flags for common server "
"applications, such as SQUID, FTPd, SSHd, SMTP & POP Daemons."
msgstr "Kontrolli aktiv Lloji nga Shërbimi për server SMTP POP."

#: src/wizard.c:524
#, fuzzy
msgid "The X Window System"
msgstr "X Dritarja Sistemi"

#: src/wizard.c:528
#, fuzzy
msgid ""
"Selecting this option will automatically configure Type of Service for "
"`Throughput' for both X and SSHd. You should select this ONLY if you need to "
"run X over your link"
msgstr "konfiguro Lloji nga Shërbimi për për X dhe Ti zgjidh X lidhje"

#: src/wizard.c:537
msgid "Throughput"
msgstr ""

#: src/wizard.c:541
#, fuzzy
msgid ""
"Selecting this option will configure Type of Service flags for Maximum "
"Throughput for the options you have selected."
msgstr "konfiguro Lloji nga Shërbimi për për."

#: src/wizard.c:545
msgid "Reliability"
msgstr ""

#: src/wizard.c:549
#, fuzzy
msgid ""
"Selecting this option will configure Type of Service flags for Maximum "
"Reliability for the options you have selected."
msgstr "konfiguro Lloji nga Shërbimi për për."

#: src/wizard.c:553
msgid "Delay"
msgstr "Vonesa"

#: src/wizard.c:557
#, fuzzy
msgid ""
"Selecting this option will configure Type of Service flags for Minimum Delay "
"for the options you have selected."
msgstr "konfiguro Lloji nga Shërbimi për Vonesa për."

#: src/wizard.c:590
#, fuzzy
msgid ""
"This wizard will help you to set up a firewall for your\n"
"Linux machine. You will be asked some questions\n"
"about your network setup in order to customize the\n"
"firewall for your system.\n"
"\n"
msgstr "ndihmë sipër a për Ti në për sistemi j j"

#: src/wizard.c:595
#, fuzzy
msgid ""
"Tip: If you are uncertain of how to answer a question it is\n"
"best to use the default value supplied.\n"
"\n"
msgstr "Tip nga a është prezgjedhur j j"

#: src/wizard.c:598
#, fuzzy
msgid "Please press the forward button to continue.\n"
msgstr "j"

#: src/wizard.c:690 src/wizard.c:707
msgid "Please review your choices"
msgstr ""

#: src/wizard.c:691
#, fuzzy
msgid "The local area and the Internet connected devices can not be the same."
msgstr "lokal dhe Internet nuk."

#: src/wizard.c:708
#, fuzzy
msgid "The supplied DHCP configuration is not valid."
msgstr "është nuk."

#: src/wizard.c:768
#, fuzzy
msgid "Firewall Wizard"
msgstr "Firewall Asistenti"

#: src/wizard.c:793
#, fuzzy
msgid "Welcome to Firestarter"
msgstr "Mirësevini Firestarter"

#: src/wizard.c:799
#, fuzzy
msgid "Network device setup"
msgstr "Rrjeti periferik"

#: src/wizard.c:805
#, fuzzy
msgid "Internet connection sharing setup"
msgstr "Internet"

#. FIXME: ToS and ICMP filtering are no longer part of the wizard
#. Maybe the code should be moved to the preferences module?
#: src/wizard.c:818
#, fuzzy
msgid "Type of Service filtering setup"
msgstr "Lloji nga Shërbimi"

#. g_ptr_array_add (wizard->pages, page);
#: src/wizard.c:824
msgid "ICMP filtering setup"
msgstr ""

#: src/wizard.c:832
#, fuzzy
msgid "Ready to start your firewall"
msgstr "Gati nisje"

#: src/wizard.c:833
#, fuzzy
msgid ""
"The wizard is now ready to start your firewall.\n"
"\n"
"Press the save button to continue, or the back button\n"
"to review your choices.\n"
"\n"
msgstr "është tani nisje j ruaj mbrapa j j"

