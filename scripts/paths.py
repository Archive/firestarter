####
# File path constants
#
#   http://www.fs-security.com
#   (C) 2005, released under the GPL version 2
#
####

# System files
IP="/sbin/ip"
IPTABLES="/sbin/iptables"
MODPROBE="/sbin/modprobe"
SHELL="/bin/sh"
RESOLV="/etc/resolv.conf"

# Firestarter directories
BASE_DIR="/etc/firestarter/"
INBOUND_RULES_DIR=BASE_DIR + "inbound/"
OUTBOUND_RULES_DIR=BASE_DIR + "outbound/"

# Rule files
FILTER_PORTS=BASE_DIR + "events-filter-ports"
FILTER_HOSTS=BASE_DIR + "events-filter-hosts"
NON_ROUTABLES=BASE_DIR + "non-routables"
USER_PRE=BASE_DIR + "user-pre"
USER_POST=BASE_DIR + "user-post"
FORWARD=INBOUND_RULES_DIR + "forward"
INBOUND_ALLOW_FROM=INBOUND_RULES_DIR + "allow-from"
INBOUND_ALLOW_SERVICE=INBOUND_RULES_DIR + "allow-service"
OUTBOUND_DENY_TO=OUTBOUND_RULES_DIR + "deny-to"
OUTBOUND_DENY_FROM=OUTBOUND_RULES_DIR + "deny-from"
OUTBOUND_DENY_SERVICE=OUTBOUND_RULES_DIR + "deny-service"
OUTBOUND_ALLOW_TO=OUTBOUND_RULES_DIR + "allow-to"
OUTBOUND_ALLOW_FROM=OUTBOUND_RULES_DIR + "allow-from"
OUTBOUND_ALLOW_SERVICE=OUTBOUND_RULES_DIR + "allow-service"
