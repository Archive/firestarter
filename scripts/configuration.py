#### Firestarter 1.1.0 configuration file
#
#   The Firestarter configuration file can either be modified by hand
#   or by using the Firestarter graphical interface
#
#   http://www.fs-security.com
#   (C) 2005, released under the GPL version 2
#
####

from interface import *

#### --(Network Interfaces)--
# This section defines the network interfaces in the firewall host and the
# type of networks they are connected to. There is no limit to the number
# of interfaces you can define.
#
# FORMAT:
#   Interface("NAME", network.INTERNET[LOCAL, profile.UNTRUSTED|TRUSTED|IGNORED)
#
# EXAMPLES:
#   External network interface:
#     Interface("eth0", network.INTERNET, profile.UNTRUSTED)
#   Internal network interface:
#     Interface("eth1", network.LOCAL, profile.UNTRUSTED)
#   Trusted internal network interface:
#     Interface("eth2", network.LOCAL, profile.TRUSTED)

#Interface("eth0", network.INTERNET, profile.UNTRUSTED) # UNCOMMENT THIS LINE

#### --(Internet Connection Sharing)--
# Manage DHCP server for local network clients
DHCP_SERVER=True
# Forward server's DNS settings to clients in DHCP lease
DHCP_DYNAMIC_DNS=True

#### --(Inbound Traffic)--
# Packet rejection method
#   DROP:   Ignore the packet
#   REJECT: Send back an error packet in response
STOP_TARGET="DROP"

#### --(Outbound Traffic)--
# Default Outbound Traffic Policy
#   permissive: everything not denied is allowed
#   restrictive everything not allowed is denied
OUTBOUND_POLICY="permissive"

#### --(Type of Service)--
# Enable ToS filtering
FILTER_TOS=False

# Apply ToS to typical client tasks such as SSH and HTTP
TOS_CLIENT=False
# Apply ToS to typical server tasks such as SSH, HTTP, HTTPS and POP3
TOS_SERVER=False
# Apply ToS to Remote X server connections
TOS_X=False
# ToS parameters
#   4:  Maximize Reliability
#   8:  Maximize-Throughput
#   16: Minimize-Delay
TOSOPT="8"

#### --(ICMP Filtering)--
# Enable ICMP filtering
FILTER_ICMP=False

# Allow Echo requests
ICMP_ECHO_REQUEST=True
# Allow Echo replies
ICMP_ECHO_REPLY=True
# Allow Traceroute requests
ICMP_TRACEROUTE=True
# Allow MS Traceroute Requests
ICMP_MSTRACEROUTE=True
# Allow Unreachable Requests
ICMP_UNREACHABLE=True
# Allow Timestamping Requests
ICMP_TIMESTAMPING=True
# Allow Address Masking Requests
ICMP_MASKING=True
# Allow Redirection Requests
ICMP_REDIRECTION=True
# Allow Source Quench Requests
ICMP_SOURCE_QUENCHES=True

#### --(Broadcast Traffic)--
# Block external broadcast traffic
BLOCK_EXTERNAL_BROADCAST=True
# Block internal broadcast traffic
BLOCK_INTERNAL_BROADCAST=False

#### --(Traffic Validation)--
# Block non-routable traffic on the public interfaces
BLOCK_NON_ROUTABLES=False

#### --(Logging)--
# System log level
LOG_LEVEL="INFO"
