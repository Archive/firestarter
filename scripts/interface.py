####
# Network interface representation
#
#   http://www.fs-security.com
#   (C) 2005, released under the GPL version 2
#
####

import paths
import os
from util import *

ext_interfaces = []
int_interfaces = []

# Environment variables available to subprocesses
os.environ['IP'] =  paths.IP # The path to the "ip" program

# Enumerations
class network:
    INTERNET, LOCAL = range(2)
class profile:
    UNTRUSTED, TRUSTED, IGNORED = range(3)

class Interface:

    def __init__(self, name, network_type, firewall_profile):
        self.name = name
        self.profile = firewall_profile
        self.ip = get_ip(name)
        self.broadcast = get_broadcast(name)
        self.network = get_network(name)

        if not self.ip:
            warn("Configured interface "+ name + " is not active")
            return

        if self.profile == profile.IGNORED:
            return
        
        if network_type == network.INTERNET:
            ext_interfaces.append(self)
        elif network_type == network.LOCAL:
            int_interfaces.append(self)

    def __str__(self):
        return self.name

def get_ip(name):
    ip = v('$IP addr show label ' + name + ' | gawk \'/inet /{ gsub("/.*","",$0) ; print $2 }\'')
    return ip

def get_broadcast(name):
    bcast = v('$IP addr show label ' + name + ' | gawk \'/inet /{ print $4 }\'')
    return bcast

def get_network(name):
    net = v('$IP addr show label ' + name + ' | gawk \'/inet /{ print $2 }\'')
    return net
