####
# Firestarter util module
#
#   Small utilitary functions, mainly for source code readability
#
#   http://www.fs-security.com
#   (C) 2005, released under the GPL
#
####

import os
import sys
from os import popen

def e(command):
    "Execute a command, showing any output"
    p = popen(command)
    o = p.read()
    if o:
        print o
    return p.close()

def v(command):
    "Grab the output value of a command"
    return popen(command).read().strip()

def die(error):
    "Print the error message and exit with a return value of 1"
    sys.exit(error)

def warn(error):
    "Print a warning message"
    print 'Warning:', error

def touch(file):
    "Create a file"
    try:
        f = open(file, "w")
        f.close()
    except IOError, e:
        die("Could not create file %s: %s" % (file, e))

def file_exists(file):
    "Test if a file exists"
    return os.access(file, os.R_OK)
