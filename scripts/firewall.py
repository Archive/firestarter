####
# Firestarter netfilter module
#
#   The Firestarter firewall functions. Do not invoke directly,
#   run the firestarter.py control script instead
#
#   http://www.fs-security.com
#   (C) 2005, released under the GPL version 2
#
####

from configuration import *
from interface import *
from util import *
import paths

# Features the firewall host supports, set by check_capabilities()
log_supported = False          # The LOG target is supported
nat_supported = False          # The nat table exists
mangle_supported = False       # The mangle table exists
stripoptions_supported = False # The IPV4OPTSSTRIP target is supported

# Environment variables available to subprocesses
os.environ['IPT'] = paths.IPTABLES       # The path to iptables
os.environ['MPB'] = paths.MODPROBE       # The path to modprobe
os.environ['STOP_TARGET'] = STOP_TARGET # The default stop target (DROP or REJECT)
os.environ['LOG_LEVEL'] = LOG_LEVEL     # The default syslog level
os.environ['TOSOPT'] = TOSOPT           # The ToS profile to apply

def reset_chains():
    "Clear all builtin chains"

    # Purge standard chains (INPUT, OUTPUT, FORWARD)
    e("$IPT -F")
    e("$IPT -X")
    e("$IPT -Z")

    # Purge extended chains (MANGLE & NAT) if they exist
    if mangle_supported:
        e("$IPT -t mangle -F")
        e("$IPT -t mangle -X")
        e("$IPT -t mangle -Z")

    if nat_supported:
        e("$IPT -t nat -F")
        e("$IPT -t nat -X")
        e("$IPT -t nat -Z")

def stop_firewall():
    reset_chains()
    e("$IPT -P INPUT ACCEPT")
    e("$IPT -P FORWARD ACCEPT")
    e("$IPT -P OUTPUT ACCEPT")

def lock_firewall():
    "Lock the firewall, denying all traffic"

    e("$IPT -P INPUT DROP")
    e("$IPT -P FORWARD DROP")
    e("$IPT -P OUTPUT DROP")
    e("$IPT -F")
    e("$IPT -X")
    e("$IPT -Z")

def load_modules():
    """Try to load the kernel modules we need
       Failure does not matter, the actual capabilites are checked later on"""

    e("$MPB ip_tables 2> /dev/null ")
    e("$MPB iptable_filter 2> /dev/null")
    e("$MPB ipt_state 2> /dev/null")
    e("$MPB ip_conntrack 2> /dev/null")
    e("$MPB ip_conntrack_ftp 2> /dev/null")
    e("$MPB ip_conntrack_irc 2> /dev/null")
    e("$MPB ipt_REJECT 2> /dev/null")
    e("$MPB ipt_TOS 2> /dev/null")
    e("$MPB ipt_MASQUERADE 2> /dev/null")
    e("$MPB ipt_LOG 2> /dev/null")
    e("$MPB iptable_mangle 2> /dev/null")
    e("$MPB ipt_ipv4optsstrip 2> /dev/null")
    if int_interfaces:
        e("$MPB iptable_nat 2> /dev/null")
        e("$MPB ip_nat_ftp 2> /dev/null")
        e("$MPB ip_nat_irc 2> /dev/null")

def capabilities_check():
    "Check which features can be used in the firewall"

    # The feature flags
    global log_supported, nat_supported, mangle_supported, stripoptions_supported 
    
    # Make sure the test chains don't exist
    e("$IPT -F test 2> /dev/null")
    e("$IPT -X test 2> /dev/null")
    if int_interfaces:
        e("$IPT -t nat -F test 2> /dev/null")
        e("$IPT -t nat -X test 2> /dev/null")

    # Iptables support check, mandatory feature
    if e("$IPT -N test 2>&1") != None:
        print "Fatal error: Your kernel does not support iptables"
        sys.exit(100)

    # Logging support check
    if e("$IPT -A test -j LOG 2>&1") == None:
        log_supported=True
    else:
        warn("Logging not supported by kernel, you will receive no firewall event updates.")

    # NAT support check
    if int_interfaces:
        if e("$IPT -t nat -N test 2>&1") == None:
            nat_supported=True
        else:
            warn("Network address translation not supported by kernel, feature disabled.")

    # Mangle support check
    if e("$IPT -t mangle -F 2>&1") == None:
        mangle_supported=True
    else:
        warn("Packet mangling not supported by kernel, feature disabled.")

    # IP options stripping support check (all output supressed)
    if e("$IPT -t mangle -A test -j IPV4OPTSSTRIP 2>/dev/null") == None:
        stripoptions_supported=True
        # Stripoptions is pretty rare, so don't bother reporting failure

def execute_firewall():
    "(Re)load the entire firewall from scratch"

    # Check that a network configuration has been specified
    if len (ext_interfaces) == 0:
        print "Firestarter is not yet configured"
        sys.exit(1)

    load_modules()
    capabilities_check()
    reset_chains()

    # --------( Chain Configuration - Configure Default Policy )--------

    # Configure standard chains (INPUT, OUTPUT, FORWARD).
    e("$IPT -P INPUT DROP")
    e("$IPT -P OUTPUT DROP")
    e("$IPT -P FORWARD DROP")

    # Configure extended chains (MANGLE & NAT) if required.
    if mangle_supported:
        e("$IPT -t mangle -P INPUT ACCEPT")
        e("$IPT -t mangle -P OUTPUT ACCEPT")
        e("$IPT -t mangle -P PREROUTING ACCEPT")
        e("$IPT -t mangle -P POSTROUTING ACCEPT")
    if nat_supported:
        e("$IPT -t nat -P OUTPUT ACCEPT")
        e("$IPT -t nat -P PREROUTING ACCEPT")
        e("$IPT -t nat -P POSTROUTING ACCEPT")


    # --------( Chain Configuration - Create Default Result Chains )--------

    # Create a new chain for filtering the input before logging is performed
    e("$IPT -N LOG_FILTER 2> /dev/null")
    e("$IPT -F LOG_FILTER")

    # Hosts for which logging is disabled
    f = open(paths.FILTER_HOSTS, "r")
    for host in f:
        e("$IPT -A LOG_FILTER -s %s -j $STOP_TARGET" % host.rstrip())
    f.close()

    # Ports for which logging is disabled
    f = open(paths.FILTER_PORTS, "r")
    for port in f:
        p = port.rstrip()
        e("$IPT -A LOG_FILTER -p tcp --dport %s -j $STOP_TARGET" % p)
        e("$IPT -A LOG_FILTER -p udp --dport %s -j $STOP_TARGET" % p)
    f.close()

    # Broadcast filter
    if BLOCK_EXTERNAL_BROADCAST:
        for i in ext_interfaces:
            e("$IPT -A LOG_FILTER -i %s -d 255.255.255.255 -j $STOP_TARGET" % i)
            if i.broadcast:
                e("$IPT -A LOG_FILTER -d %s -j $STOP_TARGET" % i.broadcast)

    if int_interfaces and BLOCK_INTERNAL_BROADCAST:
        for i in int_interfaces:
            e("$IPT -A LOG_FILTER -i %s -d 255.255.255.255 -j $STOP_TARGET" % i)
            if i.broadcast:
                e("$IPT -A LOG_FILTER -i %s -d %s -j $STOP_TARGET" % (i, i.broadcast))

    # Create a new log and stop input (LSI) chain.
    e("$IPT -N LSI 2> /dev/null")
    e("$IPT -F LSI")
    e("$IPT -A LSI -j LOG_FILTER")
    if log_supported:
	# Syn-flood protection
	e('$IPT -A LSI -p tcp --syn -m limit --limit 1/s -j LOG --log-level=$LOG_LEVEL --log-prefix "Inbound "')
	e('$IPT -A LSI -p tcp --syn -j $STOP_TARGET')
	# Rapid portscan protection
	e('$IPT -A LSI -p tcp --tcp-flags SYN,ACK,FIN,RST RST -m limit --limit 1/s -j LOG --log-level=$LOG_LEVEL --log-prefix "Inbound "')
	e('$IPT -A LSI -p tcp --tcp-flags SYN,ACK,FIN,RST RST -j $STOP_TARGET')
	# Ping of death protection
	e('$IPT -A LSI -p icmp --icmp-type echo-request -m limit --limit 1/s -j LOG --log-level=$LOG_LEVEL --log-prefix "Inbound "')
	e('$IPT -A LSI -p icmp --icmp-type echo-request -j $STOP_TARGET')
	# Log everything
	e('$IPT -A LSI -m limit --limit 5/s -j LOG --log-level=$LOG_LEVEL --log-prefix "Inbound "')
    e('$IPT -A LSI -j $STOP_TARGET') # Terminate evaluation

    # Create a new log and stop output (LSO) chain.
    e('$IPT -N LSO 2> /dev/null')
    e('$IPT -F LSO')
    e('$IPT -A LSO -j LOG_FILTER')
    if log_supported:
	# Log everything
	e('$IPT -A LSO -m limit --limit 5/s -j LOG --log-level=$LOG_LEVEL --log-prefix "Outbound "')
    e('$IPT -A LSO -j REJECT') # Terminate evaluation


    # --------( Initial Setup - Nameservers )--------

    # Allow regular DNS traffic
    f = open(paths.RESOLV)
    for line in f:
        items = line.split()
        if len(items) > 1 and items[0] == "nameserver":
            server = items[1]
            e("$IPT -A INPUT -p tcp ! --syn -s %s -d 0/0 -j ACCEPT" % server)
            e("$IPT -A INPUT -p udp -s %s -d 0/0 -j ACCEPT" % server)

            for i in ext_interfaces:
                e("$IPT -A OUTPUT -p tcp -s %s -d %s --dport 53 -j ACCEPT" % (i.ip, server))
                e("$IPT -A OUTPUT -p udp -s %s -d %s --dport 53 -j ACCEPT" % (i.ip, server))
    f.close()


    # TODO: SYSCTL

    # Run the user supplied pre script
    execute_user_script(paths.USER_PRE)

    # Allow all traffic on the loopback interface
    e('$IPT -A INPUT -i lo -s 0/0 -d 0/0 -j ACCEPT')
    e('$IPT -A OUTPUT -o lo -s 0/0 -d 0/0 -j ACCEPT')

    # Allow all traffic on the trusted interfaces
    for i in int_interfaces:
        if i.profile == profile.TRUSTED:
            e("$IPT -A INPUT -i %s -j ACCEPT" % i)

    # --------( Rules Configuration - Type of Service (ToS) - Ruleset Filtered by GUI )--------
    if FILTER_TOS:
        if TOS_CLIENT and mangle_supported:
		# ToS: Client Applications
		e('$IPT -t mangle -A OUTPUT -p tcp -j TOS --dport 20:21 --set-tos $TOSOPT')
		e('$IPT -t mangle -A OUTPUT -p tcp -j TOS --dport 22 --set-tos $TOSOPT')
		e('$IPT -t mangle -A OUTPUT -p tcp -j TOS --dport 68 --set-tos $TOSOPT')
		e('$IPT -t mangle -A OUTPUT -p tcp -j TOS --dport 80 --set-tos $TOSOPT')
		e('$IPT -t mangle -A OUTPUT -p tcp -j TOS --dport 443 --set-tos $TOSOPT')
	if TOS_SERVER and mangle_supported:
		# ToS: Server Applications
		e('$IPT -t mangle -A OUTPUT -p tcp -j TOS --dport 20:21 --set-tos $TOSOPT')
		e('$IPT -t mangle -A OUTPUT -p tcp -j TOS --dport 22 --set-tos $TOSOPT')
		e('$IPT -t mangle -A OUTPUT -p tcp -j TOS --dport 25 --set-tos $TOSOPT')
		e('$IPT -t mangle -A OUTPUT -p tcp -j TOS --dport 53 --set-tos $TOSOPT')
		e('$IPT -t mangle -A OUTPUT -p tcp -j TOS --dport 67 --set-tos $TOSOPT')
		e('$IPT -t mangle -A OUTPUT -p tcp -j TOS --dport 80 --set-tos $TOSOPT')
		e('$IPT -t mangle -A OUTPUT -p tcp -j TOS --dport 110 --set-tos $TOSOPT')
		e('$IPT -t mangle -A OUTPUT -p tcp -j TOS --dport 143 --set-tos $TOSOPT')
		e('$IPT -t mangle -A OUTPUT -p tcp -j TOS --dport 443 --set-tos $TOSOPT')
		e('$IPT -t mangle -A OUTPUT -p tcp -j TOS --dport 1812 --set-tos $TOSOPT')
		e('$IPT -t mangle -A OUTPUT -p tcp -j TOS --dport 1813 --set-tos $TOSOPT')
		e('$IPT -t mangle -A OUTPUT -p tcp -j TOS --dport 2401 --set-tos $TOSOPT')
		e('$IPT -t mangle -A OUTPUT -p tcp -j TOS --dport 8080 --set-tos $TOSOPT')
	if TOS_X and mangle_supported:
		# ToS: The X Window System
		e('$IPT -t mangle -A OUTPUT -p tcp -j TOS --dport 22 --set-tos 0x10')
		e('$IPT -t mangle -A OUTPUT -p tcp -j TOS --dport 6000:6015 --set-tos 0x08')


    # --------( Rules Configuration - ICMP )--------

    if FILTER_ICMP:
        if ICMP_ECHO_REQUEST:
            # ICMP: Ping Requests
            e('$IPT -A INPUT -p icmp --icmp-type echo-request -m limit --limit 1/s -j ACCEPT')
            e('$IPT -A FORWARD -p icmp --icmp-type echo-request -m limit --limit 1/s -j ACCEPT')

        if ICMP_ECHO_REPLY:
            # ICMP: Ping Replies
            e('$IPT -A INPUT -p icmp --icmp-type echo-reply -m limit --limit 1/s -j ACCEPT')
            e('$IPT -A FORWARD -p icmp --icmp-type echo-reply -m limit --limit 1/s -j ACCEPT')
            
        if ICMP_TRACEROUTE:
            # ICMP: Traceroute Requests
            e('$IPT -A INPUT -p udp --dport 33434 -j ACCEPT')
            e('$IPT -A FORWARD -p udp --dport 33434 -j ACCEPT')
        else:
            e('$IPT -A INPUT -p udp --dport 33434 -j LSI')
            e('$IPT -A FORWARD -p udp --dport 33434 -j LSI')

        if ICMP_MSTRACEROUTE:
            # ICMP: MS Traceroute Requests
            e('$IPT -A INPUT -p icmp --icmp-type destination-unreachable -j ACCEPT')
            e('$IPT -A FORWARD -p icmp --icmp-type destination-unreachable -j ACCEPT')

        if ICMP_UNREACHABLE:
            # ICMP: Unreachable Requests
            e('$IPT -A INPUT -p icmp --icmp-type host-unreachable -j ACCEPT')
            e('$IPT -A FORWARD -p icmp --icmp-type host-unreachable -j ACCEPT')

        if ICMP_TIMESTAMPING:
            # ICMP: Timestamping Requests
            e('$IPT -A INPUT -p icmp --icmp-type timestamp-request -j ACCEPT')
            e('$IPT -A INPUT -p icmp --icmp-type timestamp-reply -j ACCEPT')

        if ICMP_MASKING:
            # ICMP: Address Masking
            e('$IPT -A INPUT -p icmp --icmp-type address-mask-request -j ACCEPT')
            e('$IPT -A INPUT -p icmp --icmp-type address-mask-reply -j ACCEPT')
            e('$IPT -A FORWARD -p icmp --icmp-type address-mask-request -j ACCEPT')
            e('$IPT -A FORWARD -p icmp --icmp-type address-mask-reply -j ACCEPT')

        if ICMP_REDIRECTION:
            # ICMP: Redirection Requests
            e('$IPT -A INPUT -p icmp --icmp-type redirect -m limit --limit 2/s -j ACCEPT')
            e('$IPT -A FORWARD -p icmp --icmp-type redirect -m limit --limit 2/s -j ACCEPT')

        if ICMP_SOURCE_QUENCHES:
            # ICMP: Source Quench Requests
            e('$IPT -A INPUT -p icmp --icmp-type source-quench -m limit --limit 2/s -j ACCEPT')
            e('$IPT -A FORWARD -p icmp --icmp-type source-quench -m limit --limit 2/s -j ACCEPT')

        # Catch ICMP traffic not allowed above
        e('$IPT -A INPUT -p icmp -j LSI')
        e('$IPT -A FORWARD -p icmp -j LSI')
    else:
        # Allow all ICMP traffic when filtering disabled
        e('$IPT -A INPUT -p icmp -m limit --limit 10/s -j ACCEPT')
        e('$IPT -A FORWARD -p icmp -m limit --limit 10/s -j ACCEPT')


    if int_interfaces:
        # --------( Rules Configuration - Masquerading - Sysctl Modifications )--------

        #Turn on IP forwarding
        if file_exists("/proc/sys/net/ipv4/ip_forward"):
            e('echo 1 > /proc/sys/net/ipv4/ip_forward')

        # --------( Rules Configuration - Masquerading - Default Ruleset )--------

        #TCPMSS Fix - Needed for *many* broken PPPO{A/E} clients
        e('$IPT -A FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu')

	if stripoptions_supported and mangle_supported:
                #IPv4OPTIONS Fix - Strip IP options from a forwarded packet
		e('$IPT -t mangle -A PREROUTING -j IPV4OPTSSTRIP')

        # --------( Rules Configuration - Forwarded Traffic )--------

        if nat_supported:
                #Masquerade outgoing traffic
                for i in ext_interfaces:
                    e('$IPT -t nat -A POSTROUTING -o %s -j MASQUERADE' % i)

	# Services forward from the firewall to the internal network
        # FILE FORMAT: Service, external port, internal host, internal port, comment
        f = open(paths.FORWARD)
        for line in f:
            items = line.split(',')

            ext_port = clean_port(items[1]) # The port(s) on the firewall
            host = items[2]                 # The internal host to forward to
            int_port = clean_port(items[3]) # The port(s) on the target

            int_port_ipt = ipt_port(int_port)
            ext_port_ipt = ipt_port(ext_port)

            for i in EXT_INTERFACES:
                e('$IPT -A FORWARD -i %s -p tcp -d %s --dport %s -j ACCEPT' % (i, host, int_port_ipt))
                e('$IPT -A FORWARD -i %s -p udp -d %s --dport %s -j ACCEPT' % (i, host, int_port_ipt))
                e('$IPT -A PREROUTING -t nat -i %(i)s -p tcp --dport %(ext_port_ipt)s \
                  -j DNAT --to-destination %(host)s:%(int_port)s' % locals())
                e('$IPT -A PREROUTING -t nat -i %(i)s -p udp --dport %(ext_port_ipt)s \
                  -j DNAT --to-destination %(host)s:%(int_port)s' % locals())
        f.close()

    
    # --------( Rules Configuration - Traffic Prefiltering )--------

    if BLOCK_NON_ROUTABLES:
	# Block traffic from non-routable address space on the public interfaces
	e("$IPT -N NR 2> /dev/null")
	e("$IPT -F NR")

        f = open(paths.NON_ROUTABLES, "r")
        for block in f:
            for i in ext_interfaces:
                e("$IPT -A NR -s %s -d %s -i %s -j LSI" % (block.rstrip(), i.network, i))

        # Don't perform NR check if the traffic is coming from our own network,
        # which is allowed to use private address space
        for i in ext_interfaces:
            e("$IPT -A INPUT -s ! %s -i %s -j NR" % (net, i.network))

    # Block Multicast Traffic
    #  Some cable/DSL providers require their clients to accept multicast transmissions
    #  you should remove the following four rules if you are affected by multicasting
    e("$IPT -A INPUT -s 224.0.0.0/8 -d 0/0 -j DROP")
    e("$IPT -A INPUT -s 0/0 -d 224.0.0.0/8 -j DROP")
    e("$IPT -A OUTPUT -s 224.0.0.0/8 -d 0/0 -j DROP")
    e("$IPT -A OUTPUT -s 0/0 -d 224.0.0.0/8 -j DROP")

    # Block Traffic with Stuffed Routing
    #  Early versions of PUMP - (the DHCP client application included in RH / Mandrake) require
    #  inbound packets to be accepted from a source address of 255.255.255.255.  If you have issues
    #  with DHCP clients on your local LAN - either update PUMP, or remove the first rule below)
    e("$IPT -A INPUT -s 255.255.255.255 -j DROP")
    e("$IPT -A INPUT -d 0.0.0.0 -j DROP")
    e("$IPT -A OUTPUT -s 255.255.255.255 -j DROP")
    e("$IPT -A OUTPUT -d 0.0.0.0 -j DROP")

    e("$IPT -A INPUT -m state --state INVALID -j DROP") # Block Input Traffic with Invalid Flags
    e("$IPT -A OUTPUT -m state --state INVALID -j DROP #") # Block Output Traffic w/ Invalid Flags
    e("$IPT -A INPUT -f -m limit --limit 10/minute -j LSI") # Block Traffic w/ Excessive Fragmented Packets



    # --------( User Traffic Policy Setup )--------

    # Load the inbound traffic policy
    load_inbound_policy()
    for i in ext_interfaces:
        e("$IPT -A INPUT -i %s -j INBOUND" % i) # Check Internet to firewall traffic

    if int_interfaces:
        internal_ips = [i.ip for i in int_interfaces]
        external_ips = [i.ip for i in ext_interfaces]
        
        for i in int_interfaces:
            for ip in internal_ips:
                e("$IPT -A INPUT -i %s -d %s -j INBOUND" % (i, ip)) # Check LAN to firewall (private ip) traffic
            for ip in external_ips:
                e("$IPT -A INPUT -i %s -d %s -j INBOUND" % (i, ip))   # Check LAN to firewall (public ip) traffic
            if i.broadcast:
                e("$IPT -A INPUT -i %s -d %s -j INBOUND" % (i, i.broadcast)) # Check LAN to firewall broadcast traffic


    # Load the outbound traffic policy
    load_outbound_policy()
    for i in ext_interfaces:
        e("$IPT -A OUTPUT -o %s -j OUTBOUND" % i) # Check firewall to Internet traffic
    if int_interfaces:
        for i in int_interfaces:
            e("$IPT -A OUTPUT -o %s -j OUTBOUND" % i) # Check firewall to LAN traffic
            e("$IPT -A FORWARD -i %s -j OUTBOUND" % i) # Check LAN to Internet traffic

            # Allow Internet to LAN response traffic
            e("$IPT -A FORWARD -p tcp -d %s -m state --state ESTABLISHED,RELATED -j ACCEPT" % i.network)
            e("$IPT -A FORWARD -p udp -d %s -m state --state ESTABLISHED,RELATED -j ACCEPT" % i.network)


    # Run the user supplied post script
    execute_user_script(paths.USER_POST)

    return True

def load_inbound_policy():
    "(Re)load the inbound policy"

    # Initialize inbound chain
    e("$IPT -N INBOUND 2> /dev/null")
    e("$IPT -F INBOUND")

    # Allow response traffic
    e("$IPT -A INBOUND -p tcp -m state --state ESTABLISHED,RELATED -j ACCEPT")
    e("$IPT -A INBOUND -p udp -m state --state ESTABLISHED,RELATED -j ACCEPT")

    # Hosts from which connections are always allowed
    simple_rule_iterate(paths.INBOUND_ALLOW_FROM,
                        "$IPT -A INBOUND -s %s -j ACCEPT")

    # Services allowed
    f = open(paths.INBOUND_ALLOW_SERVICE)
    # FILE FORMAT: "service, ports, target, comment"
    for line in f:
        items = line.split(",")
        ports = items[1]
        target = items[2].strip()

        for port in ports.split():
            port_ipt = ipt_port(port)
            
            if target == "everyone":
                e("$IPT -A INBOUND -p tcp -s 0/0 --dport %s -j ACCEPT" % port_ipt)
                e("$IPT -A INBOUND -p udp -s 0/0 --dport %s -j ACCEPT" % port_ipt)
            elif target == "lan":
                for i in int_interfaces:
                    e("$IPT -A INBOUND -p tcp -s %s --dport %s -j ACCEPT" % (i.network, port_ipt))
                    e("$IPT -A INBOUND -p udp -s %s --dport %s -j ACCEPT" % (i.network, port_ipt))
            else: # Target is an IP, host or network
                e("$IPT -A INBOUND -p tcp -s %s --dport %s -j ACCEPT" % (target, port_ipt))
                e("$IPT -A INBOUND -p udp -s %s --dport %s -j ACCEPT" % (target, port_ipt))
    f.close()

    e("$IPT -A INBOUND -j LSI") # Default inbound policy


def load_outbound_policy():
    "(Re)load the outbound policy"

    # Initialize outbound chain
    e("$IPT -N OUTBOUND 2> /dev/null")
    e("$IPT -F OUTBOUND")

    # Allow ICMP packets out
    e("$IPT -A OUTBOUND -p icmp -j ACCEPT")

    # Allow response traffic
    e("$IPT -A OUTBOUND -p tcp -m state --state ESTABLISHED,RELATED -j ACCEPT")
    e("$IPT -A OUTBOUND -p udp -m state --state ESTABLISHED,RELATED -j ACCEPT")

    # Load the user's policy choices
    if OUTBOUND_POLICY == "permissive": # Permissive mode, blacklist traffic
        # Hosts to which traffic is denied
        # FILE FORMAT: "host, comment"
        simple_rule_iterate(paths.OUTBOUND_DENY_TO,
                            "$IPT -A OUTBOUND -d %s -j LSO")


	# Hosts from which traffic is denied
        # FILE FORMAT: "host, comment"
        simple_rule_iterate(paths.OUTBOUND_DENY_FROM,
                            "$IPT -A OUTBOUND -s %s -j LSO")

	# Services denied
        f = open(paths.OUTBOUND_DENY_SERVICE, "r")
        # FILE FORMAT: "service, ports, target, comment"
        for line in f:
            items = line.split(",")
            ports = items[1]
            target = items[2].strip()

            for port in ports.split():
                port_ipt = ipt_port(port)

                if target == "everyone":
                    e("$IPT -A OUTBOUND -p tcp -s 0/0 --dport %s -j LSO" % port_ipt)
                    e("$IPT -A OUTBOUND -p udp -s 0/0 --dport %s -j LSO" % port_ipt)
                elif target == "firewall":
                    for i in ext_interfaces:
                        e("$IPT -A OUTBOUND -p tcp -s %s --dport %s -j LSO" % (i.ip, port_ipt))
                        e("$IPT -A OUTBOUND -p udp -s %s --dport %s -j LSO" % (i.ip, port_ipt))
                elif target == "lan":
                    for i in int_interfaces:
                        e("$IPT -A OUTBOUND -p tcp -s %s --dport %s -j LSO" % (i.network, port_ipt))
                        e("$IPT -A OUTBOUND -p udp -s %s --dport %s -j LSO" % (i.network, port_ipt))
                else: # Target is an IP, host or network
                    e("$IPT -A OUTBOUND -p tcp -s %s --dport %s -j LSO" % (target, port_ipt))
                    e("$IPT -A OUTBOUND -p udp -s %s --dport %s -j LSO" % (target, port_ipt))
        f.close()

        e("$IPT -A OUTBOUND -j ACCEPT") # Default permissive policy

    else: # Restrictive policy, whitelist traffic
        # Hosts to which traffic is allowed
        # FILE FORMAT: "host, comment"
        simple_rule_iterate(paths.OUTBOUND_ALLOW_TO,
                            "$IPT -A OUTBOUND -d %s -j ACCEPT")

	# Hosts from which traffic is allowed
        # FILE FORMAT: "host, comment"
        simple_rule_iterate(paths.OUTBOUND_ALLOW_FROM,
                            "$IPT -A OUTBOUND -s %s -j ACCEPT")

        # Services allowed
        f = open(paths.OUTBOUND_ALLOW_SERVICE)
        # FILE FORMAT: "service, ports, target, comment"
        for line in f:
            items = line.split(",")
            ports = items[1]
            target = items[2].strip()

            for port in ports.split():
                port_ipt = ipt_port(port)

                if target == "everyone":
                    e("$IPT -A OUTBOUND -p tcp -s 0/0 --dport %s -j ACCEPT" % port_ipt)
                    e("$IPT -A OUTBOUND -p udp -s 0/0 --dport %s -j ACCEPT" % port_ipt)
                elif target == "firewall":
                    for i in ext_interfaces:
                        e("$IPT -A OUTBOUND -p tcp -s %s --dport %s -j ACCEPT" % (i.ip, port_ipt))
                        e("$IPT -A OUTBOUND -p udp -s %s --dport %s -j ACCEPT" % (i.ip, port_ipt))
                elif target == "lan":
                    for i in int_interfaces:
                        e("$IPT -A OUTBOUND -p tcp -s %s --dport %s -j ACCEPT" % (i.network, port_ipt))
                        e("$IPT -A OUTBOUND -p udp -s %s --dport %s -j ACCEPT" % (i.network, port_ipt))
                else: # Target is an IP, host or network
                    e("$IPT -A OUTBOUND -p tcp -s %s --dport %s -j ACCEPT" % (target, port_ipt))
                    e("$IPT -A OUTBOUND -p udp -s %s --dport %s -j ACCEPT" % (target, port_ipt))
        f.close()

        e("$IPT -A OUTBOUND -j LSO") # Default restrictive policy

##
# Convenience functions
##

def simple_rule_iterate(file_path, rule):
    "Create rules from a single-parameter-per-line file"
    f = open(file_path, "r")
    for line in f:
        parameter = line.split(",")[0]
        e(rule % parameter)
    f.close()

def execute_user_script(file_path):
    if file_exists(file_path):
        e("%s %s" % (paths.SHELL, file_path))
    else:
        warn("%s could not be run" % file_path)

def clean_port(port):
    "Remove all whitespace from a port range. Example: 20 - 22 -> 20-22"
    return port.replace(" ", "")

def ipt_port(port):
    "Convert a port range to the iptables syntax. Example: 20-22 -> 20:22"
    return port.replace("-", ":")
