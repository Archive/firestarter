#!/usr/bin/python
####
# Firestarter control script
#
#   The Firestarter firewall control script
#   usage: "firestarter.py command", where command is one of:
#      start:  Start the firewall
#      stop:   Stop the firewall
#      lock:   Lock the firewall, halting all traffic
#      status: Report if the firewall is active
#
#   http://www.fs-security.com
#   (C) 2005, released under the GPL version 2
#
####

import sys
import os
import firewall
from util import *

def set_lock(lock):
    "Set or remove the program lock file"
    if file_exists("/var/lock/subsys"):
        lock_file = "/var/lock/subsys/firestarter"
    else:
        lock_file = "/var/lock/firestarter"
    
    if lock:
        touch(lock_file)
    elif file_exists(lock_file):
        os.remove(lock_file)

def get_lock():
    "Return the status of the program lock file"
    return file_exists("/var/lock/subsys/firestarter") or \
           file_exists("/var/lock/firestarter")

def start_firewall():
    "Try to start the firewall"
    set_lock(True)
    if firewall.execute_firewall():
        print "Firewall started"
    else:
        set_lock(False)
        die("Firewall not started")

def report_status():
    "Report the status of the firewall"
    if get_lock():
        print "Firestarter is running..."
    else:
        print "Firestarter is stopped"

def print_usage():
    "Print a line describing how to invoke the script"
    script_name = sys.argv[0]
    print("usage: %s {start|stop|lock|status}" % script_name)

def root_check():
    if os.getuid() != 0:
        die("You must be root to control the firewall")

if __name__ == '__main__':
    "Script execution entry point"

    if len(sys.argv) != 2: # Script accepts exactly one parameter
        print_usage()
        die(None)

    command = sys.argv[1]
    if command == "start":
        root_check()
        start_firewall()
    elif command == "stop":
        root_check()
        firewall.stop_firewall()
        set_lock(False)
        print "Firewall stopped"
    elif command == "lock":
        root_check()
        firewall.lock_firewall()
        print "Firewall locked"
    elif command == "status":
        report_status()
    elif command == "reload-inbound-policy":
        root_check()
        firewall.load_inbound_policy()
    elif command == "reload-outbound-policy":
        root_check()
        firewall.load_outbound_policy()
    else: # An unknown command was given
        print_usage()
        die(None)

    sys.exit(0)
