/*---[ wizard.c ]------------------------------------------------------
 * Copyright (C) 2000-2005 Tomas Junnonen (majix@sci.fi)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.de
 *
 * The firewall setup wizard
 *--------------------------------------------------------------------*/
#include <glade/glade-xml.h>
#include "firestarter.h"
#include "wizard.h"
#include "scriptwriter.h"
#include "util.h"
#include "xpm/firestarter-pixbufs.h"
#include "preferences.h"
#include "dhcp-server.h"
#include "gui.h"
#include "statusview.h"
#include "devices.h"
#include "service.h"
#include "policyview.h"

static void build_shares (void);

typedef enum
{
	WIZARD_WELCOME_PAGE,
	WIZARD_NETWORK_PAGE,
	WIZARD_SHARING_PAGE,
	WIZARD_SERVICES_PAGE,
	WIZARD_FINISHED_PAGE
} Page;


typedef struct _Wizard Wizard;
struct _Wizard
{
	GtkWidget *window;
	GtkWidget *header;
	GtkWidget *notebook;
	GtkWidget *forward;
	GtkWidget *back;
	GtkWidget *save;
	GtkWidget *quit;
	GtkWidget *box_device_name;
	GtkWidget *box_network_type;
	GtkWidget *box_firewall_profile;
	GtkWidget *box_description;
	GtkWidget *box_shares;
	GtkWidget *check_overwrite_conf;
	GtkWidget *box_sharing_note;
	GtkWidget *box_dhcp_notice;
	GtkWidget *box_services;
	GtkWidget *check_start_now;
	gboolean   shares_need_rebuild;
};

static Wizard *wizard;
static GSList *devices;
static Device_setup **setups;
static gboolean *active_services;

static void
quit_wizard (void)
{
	gint i;
	GSList *device;

	gtk_widget_destroy (wizard->window);

	/* Cleanup */
	g_free (wizard);
	for (device = devices, i = 0; device != NULL; device = g_slist_next (device), i++) {
		if (!setups[i])
			continue;
		
		g_free (setups[i]->dhcp_low_ip);
		g_free (setups[i]->dhcp_high_ip);
		g_free (setups[i]);
	}
	g_free (setups);
	g_free (active_services);

	if (preferences_get_bool (PREFS_FIRST_RUN))
		exit_firestarter ();
	else
		gui_set_visibility (TRUE);

	wizard = NULL;
}

static Page
get_current_page(void)
{
	return gtk_notebook_get_current_page (GTK_NOTEBOOK (wizard->notebook));
}


/* Change the header at the top of the wizard to reflect the current page */
static void
update_header (void)
{
	gchar *label_markup;
	gchar *text;
	Page current_page = get_current_page();


	switch (current_page) {
	  case WIZARD_WELCOME_PAGE: text = g_strdup (_("Welcome to Firestarter")); break;
	  case WIZARD_NETWORK_PAGE: text = g_strdup (_("Network Setup")); break;
	  case WIZARD_SHARING_PAGE: text = g_strdup (_("Connection Sharing")); break;
	  case WIZARD_SERVICES_PAGE: text = g_strdup (_("Network Services")); break;
	  case WIZARD_FINISHED_PAGE: text = g_strdup (_("Setup Complete")); break;
	  default: text = g_strdup ("");
	}

	label_markup = g_strconcat ("<span size=\"xx-large\" weight=\"ultrabold\">",
	                            text, "</span>", NULL);

	gtk_label_set_markup (GTK_LABEL (wizard->header), label_markup);

	g_free (text);
	g_free (label_markup);
}

static void
go_back (void)
{
	int current_page = get_current_page();
	gtk_notebook_set_current_page (GTK_NOTEBOOK (wizard->notebook), --current_page); 
	update_header ();

	gtk_widget_set_sensitive (wizard->forward, TRUE);
	if (current_page == WIZARD_WELCOME_PAGE) {
		gtk_widget_set_sensitive (wizard->back, FALSE);
		gtk_widget_set_sensitive (wizard->save, FALSE);
	}
}


static void
go_forward (void)
{
	int current_page = get_current_page();

	/* Before page changes */

	if (current_page == WIZARD_NETWORK_PAGE) {

		/* Rebuild the sharing page entries if the device configuration has changed */
		if (wizard->shares_need_rebuild != FALSE) {
			gtk_container_foreach (GTK_CONTAINER (wizard->box_shares), (GtkCallback)gtk_widget_destroy, NULL);
			gtk_container_foreach (GTK_CONTAINER (wizard->box_dhcp_notice), (GtkCallback)gtk_widget_destroy, NULL);
			build_shares ();
		}
	}

	gtk_notebook_set_current_page (GTK_NOTEBOOK (wizard->notebook), ++current_page); 
	update_header ();

	/* After page changed */	

	gtk_widget_set_sensitive (wizard->back, TRUE);
	if (current_page == WIZARD_FINISHED_PAGE) {
		gtk_widget_set_sensitive (wizard->forward, FALSE);
		gtk_widget_set_sensitive (wizard->save, TRUE);
	}
}

static void
network_type_changed (GtkComboBox *combo, Device_setup *setup)
{
	setup->type = gtk_combo_box_get_active (combo);
	wizard->shares_need_rebuild = TRUE;
}

static void
firewall_profile_changed (GtkComboBox *combo, Device_setup *setup)
{
	setup->profile = gtk_combo_box_get_active (combo);
}

static void
update_security_description (GtkComboBox *combo, GtkWidget *label)
{
	Firewall_profile profile;
	gchar *level, *description, *color, *label_markup;

	profile = gtk_combo_box_get_active (combo);

	switch (profile) {
	  case FIREWALL_PROFILE_UNTRUSTED: level = g_strdup (_("High"));
	                                   description = g_strdup (_("Traffic must match policy"));
	                                   color = g_strdup ("black");
	                                   break;
	  case FIREWALL_PROFILE_TRUSTED: level = g_strdup (_("Low"));
	                                 description = g_strdup (_("Traffic is unrestricted"));
	                                 color = g_strdup ("red");
	                                 break;
	  case FIREWALL_PROFILE_IGNORED: level = g_strdup (_("None"));
	                                 description = g_strdup (_("Depends on system settings"));
	                                 color = g_strdup ("gray");
	                                 break;
	  default: level = g_strdup (""); description = g_strdup (""); color = g_strdup(""); break;
	}

	label_markup = g_strconcat (
		"<span weight=\"bold\" foreground=\"", color, "\">", level, "</span>",
		"<small>: ", description, "</small>", NULL);
	gtk_label_set_markup (GTK_LABEL (label), label_markup);

	g_free (level);
	g_free (description);
	g_free (color);
	g_free (label_markup);
}

static void
update_profile_sensitivity (GtkComboBox *type_combo, GtkWidget *profile_combo)
{
	Network_type type;

	type = gtk_combo_box_get_active (type_combo);

	if (type == NETWORK_TYPE_INTERNET) {
		gtk_widget_set_sensitive (profile_combo, FALSE);
		gtk_combo_box_set_active (GTK_COMBO_BOX (profile_combo), 0);
	} else {
		gtk_widget_set_sensitive (profile_combo, TRUE);
	}
}

/* Build the dynamic elements of the network setup page */
static void
build_devices (void)
{
	GtkWidget *label;
	GtkWidget *type_combo, *profile_combo;
	GSList *device;
	gchar *name;
	Device_setup *setup;
	int i;

	setups = malloc (sizeof (Device_setup) * g_slist_length (devices));

	for (device = devices, i = 0; device != NULL; device = g_slist_next (device), i++) {
		name = (gchar *)device->data;
		setup = devices_get_setup (name);
		
		if (!setup)
			continue;
		
		setups[i] = setup;

		label = gtk_label_new(NULL);
		gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
		gtk_label_set_markup (GTK_LABEL (label),
			devices_get_pretty_name (name, TRUE));
		gtk_widget_set_size_request (label, -1, 30);
		gtk_box_pack_start(GTK_BOX (wizard->box_device_name), label, FALSE, FALSE, 0);

		type_combo = gtk_combo_box_new_text ();
		gtk_combo_box_append_text (GTK_COMBO_BOX (type_combo), _("Internet"));
		gtk_combo_box_append_text (GTK_COMBO_BOX (type_combo), _("Local Network"));
		gtk_widget_set_size_request (type_combo, -1, 30);
		gtk_box_pack_start(GTK_BOX (wizard->box_network_type), type_combo, FALSE, FALSE, 0);
		gtk_combo_box_set_active (GTK_COMBO_BOX (type_combo), setup->type);
		g_signal_connect (type_combo, "changed",
	                          G_CALLBACK (network_type_changed), setup);

		profile_combo = gtk_combo_box_new_text ();
		gtk_combo_box_append_text (GTK_COMBO_BOX (profile_combo), _("Untrusted Network"));
		gtk_combo_box_append_text (GTK_COMBO_BOX (profile_combo), _("Trusted Network"));
		gtk_combo_box_append_text (GTK_COMBO_BOX (profile_combo), _("Ignored Network"));
		gtk_widget_set_size_request (profile_combo, -1, 30);
		gtk_box_pack_start(GTK_BOX (wizard->box_firewall_profile), profile_combo, FALSE, FALSE, 0);
		gtk_combo_box_set_active (GTK_COMBO_BOX (profile_combo), setup->profile);
		g_signal_connect (profile_combo, "changed",
	                          G_CALLBACK (firewall_profile_changed), setup);

		label = gtk_label_new(NULL);
		gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
		gtk_widget_set_size_request (label, -1, 30);
		gtk_box_pack_start(GTK_BOX (wizard->box_description), label, FALSE, FALSE, 0);

		update_security_description (GTK_COMBO_BOX (profile_combo), label);
		g_signal_connect (profile_combo, "changed",
	                          G_CALLBACK (update_security_description), label);

		/* Lock the profile to untrusted for Internet connections */
		update_profile_sensitivity (GTK_COMBO_BOX (type_combo), profile_combo);
		g_signal_connect (type_combo, "changed",
	                          G_CALLBACK (update_profile_sensitivity), profile_combo);

	}

	gtk_widget_show_all (wizard->box_device_name);
	gtk_widget_show_all (wizard->box_network_type);
	gtk_widget_show_all (wizard->box_firewall_profile);
	gtk_widget_show_all (wizard->box_description);
}

static void
manual_reference_enter (GtkWidget *widget)
{
	const gchar *text = gtk_label_get_text (GTK_LABEL (widget));
	gtk_label_set_markup (GTK_LABEL (widget), g_strconcat (
			      "<span foreground=\"red\" underline=\"single\">",
			      text, "</span>", NULL));
}

static void
manual_reference_leave (GtkWidget *widget)
{
	const gchar *text = gtk_label_get_text (GTK_LABEL (widget));
	gtk_label_set_markup (GTK_LABEL (widget), g_strconcat (
			      "<span foreground=\"blue\" underline=\"none\">",
			      text, "</span>", NULL));
}

/* Create a manual reference widget that looks like a hyperlink */
static GtkWidget*
manual_reference_new (gchar *description, gchar *url)
{
	GtkWidget *event_box = gtk_event_box_new ();
	GtkWidget *label = gtk_label_new (NULL);

	gtk_label_set_markup (GTK_LABEL (label), g_strconcat (
			      "<span foreground=\"blue\">",
			      description, "</span>", NULL));

	gtk_container_add (GTK_CONTAINER (event_box), label);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.0);

	gtk_widget_set_events (event_box, GDK_BUTTON_PRESS_MASK | GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK);
	g_signal_connect_swapped (G_OBJECT (event_box), "button_press_event",
		G_CALLBACK (gnome_url_show), url);

	g_signal_connect_swapped (G_OBJECT (event_box), "enter_notify_event",
		G_CALLBACK (manual_reference_enter), label);
	g_signal_connect_swapped (G_OBJECT (event_box), "leave_notify_event",
		G_CALLBACK (manual_reference_leave), label);

	gtk_event_box_set_above_child (GTK_EVENT_BOX (event_box), TRUE);

	return event_box;
}

static void
sharing_toggled (GtkToggleButton *toggle, Device_setup *setup)
{
	setup->share = gtk_toggle_button_get_active (toggle);
}

static void
dhcp_toggled (GtkToggleButton *toggle, Device_setup *setup)
{
	setup->dhcp = gtk_toggle_button_get_active (toggle);
}

static gboolean
dhcp_low_changed (GtkEntry *entry, Device_setup *setup)
{
	if (setup->dhcp_low_ip)
		g_free (setup->dhcp_low_ip);

	setup->dhcp_low_ip = g_strdup (gtk_entry_get_text (entry));

	return FALSE; /* Propagate signal */
}

static gboolean
dhcp_high_changed (GtkEntry *entry, Device_setup *setup)
{
	if (setup->dhcp_high_ip)
		g_free (setup->dhcp_high_ip);

	setup->dhcp_high_ip = g_strdup (gtk_entry_get_text (entry));

	return FALSE; /* Propagate signal */
}

/* Build the dynamic elements of the connection sharing page */
static void
build_shares (void)
{
	GtkWidget *label;
	GtkWidget *frame;
	GtkWidget *check_share, *check_dhcp;
	GtkWidget *dhcp_notice = NULL;
	GtkWidget *vbox, *hbox;
	GtkWidget *dhcp_expander;
	GtkWidget *dhcp_highest_ip, *dhcp_lowest_ip;
	GtkWidget *table;
	GSList *device;
	gchar *name;
	Device_setup *setup;
	gint i;

	if (dhcp_server_configuration_exists ()) {
		gtk_widget_show (wizard->check_overwrite_conf);
	} else {
		gtk_widget_hide (wizard->check_overwrite_conf);
	}

	for (device = devices, i = 0; device != NULL; device = g_slist_next (device), i++) {
		name = (gchar *)device->data;
		setup = setups[i];
		
		if (!setup)
			continue;
			
		if (setup->type != NETWORK_TYPE_LOCAL)
			continue;

		frame = gtk_frame_new (NULL);
		gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_NONE);
		label = gtk_label_new (NULL);
		gtk_label_set_markup (GTK_LABEL (label), g_strconcat ("<b>", devices_get_pretty_name(name, TRUE), "</b>", NULL));
		gtk_frame_set_label_widget (GTK_FRAME (frame), label);
		gtk_box_pack_start (GTK_BOX (wizard->box_shares), frame, FALSE, FALSE, 0);

		hbox = gtk_hbox_new (FALSE, 0);
		label = gtk_label_new ("    ");
		gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
		gtk_container_add (GTK_CONTAINER (frame), hbox);

		vbox = gtk_vbox_new (FALSE, 0);
		gtk_box_pack_start (GTK_BOX (hbox), vbox, FALSE, FALSE, 3);

		hbox = gtk_hbox_new (FALSE, 0);
		gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 3);

		check_share = gtk_check_button_new_with_label (_("Share Internet connection"));
		gtk_box_pack_start (GTK_BOX (hbox), check_share, FALSE, FALSE, 0);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check_share), setup->share);
		g_signal_connect (check_share, "toggled",
	                          G_CALLBACK (sharing_toggled), setup);

		check_dhcp = gtk_check_button_new_with_label (_("Enable DHCP service"));
		gtk_box_pack_start (GTK_BOX (hbox), check_dhcp, FALSE, FALSE, 12);
		g_signal_connect (check_dhcp, "toggled",
	                          G_CALLBACK (dhcp_toggled), setup);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check_dhcp), setup->dhcp);

		dhcp_expander = gtk_expander_new (_("DHCP details"));
		gtk_expander_set_spacing (GTK_EXPANDER (dhcp_expander), 3);
		gtk_expander_set_expanded (GTK_EXPANDER (dhcp_expander), FALSE);
		gtk_box_pack_start (GTK_BOX (vbox), dhcp_expander, FALSE, FALSE, 3);

		table = gtk_table_new (3, 5, FALSE);
		gtk_table_set_row_spacings (GTK_TABLE (table), 6);
		gtk_container_add (GTK_CONTAINER (dhcp_expander), table);

		label = gtk_label_new (_("Lowest IP address to assign:"));
		gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
		gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1,
		                  GTK_FILL, GTK_FILL, GNOME_PAD, 0);

		label = gtk_label_new (_("Highest IP address to assign:"));
		gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
		gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2,
		                  GTK_FILL, GTK_FILL, GNOME_PAD, 0);

		dhcp_lowest_ip = gtk_entry_new ();
		gtk_table_attach (GTK_TABLE (table), dhcp_lowest_ip, 1, 2, 0, 1,
		                  GTK_FILL, GTK_FILL, GNOME_PAD, 0);
		gtk_entry_set_text (GTK_ENTRY (dhcp_lowest_ip), setup->dhcp_low_ip);
		g_signal_connect (dhcp_lowest_ip, "changed",
	                          G_CALLBACK (dhcp_low_changed), setup);

		dhcp_highest_ip = gtk_entry_new ();
		gtk_table_attach (GTK_TABLE (table), dhcp_highest_ip, 1, 2, 1, 2,
		                  GTK_FILL, GTK_FILL, GNOME_PAD, 0);
		gtk_entry_set_text (GTK_ENTRY (dhcp_highest_ip), setup->dhcp_high_ip);

		g_signal_connect (dhcp_highest_ip, "changed",
	                          G_CALLBACK (dhcp_high_changed), setup);

		/* Disalbe the DHCP controls if no server binary is found */
		if (dhcp_server_exists ()) {
			gui_widget_sensitivity_sync (GTK_TOGGLE_BUTTON (check_share), check_dhcp);
			gtk_widget_set_sensitive (check_dhcp, setup->share);

			/* Disable the DHCP server details unless allowed to overwrite configuration */
			if (dhcp_server_configuration_exists ()) {
				gui_widget_sensitivity_sync (GTK_TOGGLE_BUTTON (wizard->check_overwrite_conf), dhcp_expander);
				gtk_widget_set_sensitive (dhcp_expander, FALSE);
			} else
				gtk_widget_set_sensitive (dhcp_expander, TRUE);
		} else {
			gtk_widget_set_sensitive (check_dhcp, FALSE);
			gtk_widget_set_sensitive (dhcp_expander, FALSE);
		}
	}


	gtk_widget_show_all (wizard->box_shares);

	if (dhcp_server_exists ()) {
		gtk_widget_hide_all (wizard->box_sharing_note);
	} else if (dhcp_notice == NULL) { /* The notice must only be built once */
		dhcp_notice = manual_reference_new (_("How to install a DHCP server package"),
					            "http://www.fs-security.com/docs/dhcp.php");
		gtk_box_pack_start (GTK_BOX (wizard->box_dhcp_notice), dhcp_notice, FALSE, FALSE, 3);

		gtk_widget_show_all (wizard->box_sharing_note);
	}

	wizard->shares_need_rebuild = FALSE;
}

static void
service_toggled (GtkToggleButton *check, gint i)
{
	active_services[i] = gtk_toggle_button_get_active (check);
}

/* Build the dynamic elements of the services page */
static void
build_services (void)
{
	const Service *services;
	gint n_services, i;
	GtkWidget *check;
	gchar *title;
	GtkWidget *box = NULL;
	GIOChannel* in;
	GError *error = NULL;
	gchar *line;
	gchar **tokens;

	services = service_get_user_services ();
	n_services = service_get_count ();

	/* Determine which services are enabled in the current configuration */
	active_services = calloc(n_services, sizeof (gboolean));
	in = g_io_channel_new_file (POLICY_IN_ALLOW_SERVICE, "r", &error);
	if (in == NULL) {
		g_printerr ("Error reading file %s: %s\n", POLICY_IN_ALLOW_SERVICE, error->message);
	} else {
		while (g_io_channel_read_line (in, &line, NULL, NULL, &error) == G_IO_STATUS_NORMAL) {
			if (g_str_has_prefix (g_strstrip (line), "#")) { /* Skip comments */
				g_free (line);
				continue;
			}

			tokens = g_strsplit_set (line, ",", 4); /* Split the rule entry into its components */

			for (i = 0; i < n_services; i++) {
				if (g_str_equal (services[i].name, tokens[0]) && /* Service name and target matches */
				    g_str_equal (" everyone", tokens[2])) {
					active_services[i] = TRUE; /* Mark service as being in use */
				}
			}
			g_strfreev (tokens);
			g_free (line);
		}
		
		g_io_channel_shutdown (in, FALSE, &error);
	}

	/* Build the service check buttons */
	for (i = 0; i < n_services; i++) {
		if (i % 7 == 0) {
			box = gtk_vbox_new (FALSE, 0);
			gtk_box_pack_start (GTK_BOX (wizard->box_services), box, FALSE, FALSE, 12);
		}

		title = g_strconcat (services[i].description, " (", services[i].name, ")", NULL);
		check = gtk_check_button_new_with_label (title);

		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check),
		                              active_services[i]);
		g_signal_connect (check, "toggled",
	                          G_CALLBACK (service_toggled), GINT_TO_POINTER (i));

		gtk_box_pack_start (GTK_BOX (box), check, FALSE, FALSE, 2);
		g_free (title);
	}

	gtk_widget_show_all (wizard->box_services);
}

/* Commit the settings from the wizard */
static void
save (void)
{
	GSList *device;
	Device_setup *setup;
	gint i;
	GSList *external = NULL, *internal = NULL;
	GSList *trusted = NULL, *ignored = NULL;
	gboolean start;

	for (device = devices, i = 0; device != NULL; device = g_slist_next (device), i++) {
		gchar *key;
		setup = setups[i];
		
		if (!setup)
			continue;

		/* Network setup page */
		if (setup->type == NETWORK_TYPE_INTERNET)
			external = g_slist_append (external, (gchar*)device->data);
		else if (setup->type == NETWORK_TYPE_LOCAL)
			internal = g_slist_append (internal, (gchar*)device->data);

		if (setup->profile == FIREWALL_PROFILE_TRUSTED)
			trusted = g_slist_append (trusted, (gchar *)device->data);
		else if (setup->profile == FIREWALL_PROFILE_IGNORED)
			ignored = g_slist_append (ignored, (gchar *)device->data);

		/* Connection sharing page */
		key = g_strconcat (PREFS_FW_DEVICES, device->data, "_share", NULL);
		preferences_set_bool (key, setup->share);
		g_free (key);

		key = g_strconcat (PREFS_FW_DEVICES, device->data, "_dhcp", NULL);
		preferences_set_bool (key, setup->dhcp);
		g_free (key);

		key = g_strconcat (PREFS_FW_DEVICES, device->data, "_dhcp_low_ip", NULL);
		preferences_set_string (key, setup->dhcp_low_ip);
		g_free (key);

		key = g_strconcat (PREFS_FW_DEVICES, device->data, "_dhcp_high_ip", NULL);
		preferences_set_string (key, setup->dhcp_high_ip);
		g_free (key);
	}
	preferences_set_list (PREFS_FW_EXT_INTERFACES, external);
	preferences_set_list (PREFS_FW_INT_INTERFACES, internal);
	preferences_set_list (PREFS_FW_TRUSTED_INTERFACES, trusted);
	preferences_set_list (PREFS_FW_IGNORED_INTERFACES, ignored);

	g_slist_free (external);
	g_slist_free (internal);
	g_slist_free (trusted);
	g_slist_free (ignored);

	/* Network services page */
	for (i = 0; i < service_get_count (); i++) {
		policyview_set_service (i, active_services[i]);
	}

	/* Finished page */
	start = gtk_toggle_button_get_active (
		GTK_TOGGLE_BUTTON (wizard->check_start_now));

	/* Output new configuration */
	scriptwriter_output_scripts ();

	if (start) {
		start_firewall ();
	}
	quit_wizard ();
}

static void
log_handler_cb (const gchar *log_domain, GLogLevelFlags
                log_level, const gchar *message, gpointer user_data)
{
	return;
}

/* [ run_wizard ]
 * Run the firewall wizard
 */
void
run_wizard (void)
{
	GladeXML *gui;
	GdkPixbuf *pixbuf;
	GdkPixbuf *logobuf;
	GtkWidget *image;

	if (wizard != NULL) {
		gtk_window_present (GTK_WINDOW (wizard->window));
		return;
	}

	g_log_set_always_fatal (G_LOG_LEVEL_WARNING);

	devices = devices_get_names ();

	/* Suppress libglade warnings */
	g_log_set_handler ("libglade", 
                           G_LOG_LEVEL_WARNING,
		    	   log_handler_cb, 
			   NULL);

	/* Try to load the interface from the current directory first */
	gui = glade_xml_new ("wizard.glade", NULL, NULL);
	if (gui == NULL) { /* If that fails, load the shared interface file */
		gui = glade_xml_new (GLADEDIR"/wizard.glade", NULL, NULL);
	}

	if (gui == NULL) {
		error_dialog (_("Missing file"),
		              _("Firestarter interface file not found"),
			      _("The interface markup file wizard.glade could not be found."),
			      gui_main_window);
		return;
	}

	wizard = g_new0 (Wizard, 1);
	
	wizard->window = glade_xml_get_widget (gui, "window");
	gtk_window_set_resizable (GTK_WINDOW (wizard->window), FALSE);

	wizard->header = glade_xml_get_widget (gui, "header");
	wizard->notebook = glade_xml_get_widget (gui, "notebook");
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (wizard->notebook), FALSE);

	update_header();
	
	/* Set up images */
	logobuf = gdk_pixbuf_new_from_inline (-1, logo, FALSE, NULL);
	image = glade_xml_get_widget (gui, "image_logo");
	gtk_image_set_from_pixbuf (GTK_IMAGE (image), logobuf);
	g_object_unref (G_OBJECT(logobuf));

	pixbuf = gdk_pixbuf_new_from_inline (-1, pengo, FALSE, NULL);
	image = glade_xml_get_widget (gui, "image_welcome");
	gtk_image_set_from_pixbuf (GTK_IMAGE (image), pixbuf);
	g_object_unref (G_OBJECT(pixbuf));

	/* Set up buttons */
	wizard->back = glade_xml_get_widget (gui, "button_back");
	gtk_widget_set_sensitive (wizard->back, FALSE);
	g_signal_connect_swapped (wizard->back, "clicked",
	                  G_CALLBACK (go_back), wizard);
	wizard->forward = glade_xml_get_widget (gui, "button_forward");
	g_signal_connect_swapped (wizard->forward, "clicked",
	                  G_CALLBACK (go_forward), wizard);
	wizard->save = glade_xml_get_widget (gui, "button_save");
	g_signal_connect_swapped (wizard->save, "clicked",
	                  G_CALLBACK (save), NULL);
	gtk_widget_set_sensitive (wizard->save, FALSE);
	wizard->quit = glade_xml_get_widget (gui, "button_quit");
	g_signal_connect_swapped (wizard->quit, "clicked",
	                  G_CALLBACK (quit_wizard), wizard);

	/* Network setup page setup */
	wizard->box_device_name = glade_xml_get_widget (gui, "box_device_name");
	wizard->box_network_type = glade_xml_get_widget (gui, "box_network_type");
	wizard->box_firewall_profile = glade_xml_get_widget (gui, "box_firewall_profile");
	wizard->box_description = glade_xml_get_widget (gui, "box_description");
	build_devices ();

	/* Connection sharing page setup */
	wizard->box_shares = glade_xml_get_widget (gui, "box_shares");
	wizard->box_sharing_note = glade_xml_get_widget (gui, "box_sharing_note");
	wizard->box_dhcp_notice = glade_xml_get_widget (gui, "box_dhcp_notice");
	wizard->check_overwrite_conf = glade_xml_get_widget (gui, "check_overwrite_conf");
	wizard->shares_need_rebuild = FALSE;
	build_shares ();

	/* Services page setup */
	wizard->box_services = glade_xml_get_widget (gui, "box_services");
	build_services ();

	/* Finished page setup */
	wizard->check_start_now = glade_xml_get_widget (gui, "check_start_now");
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (wizard->check_start_now), TRUE);
}
