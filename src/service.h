/*---[ service.h ]----------------------------------------------------
 * Copyright (C) 2000-2004 Tomas Junnonen (majix@sci.fi)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Return service used based on given port.
 *--------------------------------------------------------------------*/

#ifndef _FIRESTARTER_SERVICE
#define _FIRESTARTER_SERVICE

#include <config.h>
#include <gnome.h>

enum {
	SERVICE_NAME,
	SERVICE_DESCRIPTION,
	SERVICE_PORTS,
};

typedef struct
{
	gchar *name;
	gchar *description;
	gchar *ports;
} Service;

const Service *service_get_user_services (void);

gint service_get_count (void);

GtkListStore* service_get_model (void);

gchar *service_get_name (gint port, const gchar *proto);

gchar *service_get_icmp_name (gint type);

#endif
