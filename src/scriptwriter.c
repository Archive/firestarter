/*---[ scriptwriter.c ]-----------------------------------------------
 * Copyright (C) 2000-2005 Tomas Junnonen (majix@sci.fi)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Creates the configuration script, based on the wizard selections.
 *--------------------------------------------------------------------*/

#include <config.h>
#include <gnome.h>

#include "util.h"
#include "scriptwriter.h"
#include "preferences.h"
#include "dhcp-server.h"
#include "devices.h"

#define PPP_HOOK_FILE "/etc/ppp/ip-up.local"
const gchar* FIRESTARTER_HOOK = "sh "FIRESTARTER_CONTROL_SCRIPT" start\n";

static const gchar *
test_bool (const gchar *conf_key)
{
	static gchar *on = "True";
	static gchar *off = "False";

	if (preferences_get_bool (conf_key))
		return on;
	else
		return off;
}

void
scriptwriter_output_configuration ()
{
	gchar *path = FIRESTARTER_CONFIGURATION_SCRIPT;
	FILE *f = fopen (path, "w");
	GSList *devices, *device;
	Device_setup *setup;
	static gchar *type_internet = "network.INTERNET";
	static gchar *type_local = "network.LOCAL";
	static gchar *profile_untrusted = "profile.UNTRUSTED";
	static gchar *profile_trusted = "profile.TRUSTED";
	static gchar *profile_ignored = "profile.IGNORED";
	gchar *name, *type, *profile;

	devices = devices_get_names ();

        if (f == NULL) {
                perror(path);
                g_printerr("Script not written!");
		return;
	}
	chmod (path, 00440);

	fprintf (f, "#### Firestarter %s configuration file\n"
	            "#\n"
	            "#   The Firestarter configuration file can either be modified by hand\n"
	            "#   or by using the Firestarter graphical interface\n"
	            "#\n"
	            "#   http://www.fs-security.com\n"
	            "#   (C) 2005, released under the GPL version 2\n"
	            "#\n"
	            "####\n\n", VERSION);

	fprintf (f, "from interface import *\n\n");

	fprintf (f, "#### --(Network Interfaces)--\n"
	            "# This section defines the network interfaces in the firewall host and the\n"
	            "# type of networks they are connected to. There is no limit to the number\n"
	            "# of interfaces you can define.\n"
	            "#\n"
	            "# FORMAT:\n"
	            "#   Interface(\"NAME\", network.INTERNET[LOCAL, profile.UNTRUSTED|TRUSTED|IGNORED)\n"
	            "#\n"
	            "# EXAMPLES:\n"
	            "#   External network interface:\n"
	            "#     Interface(\"eth0\", network.INTERNET, profile.UNTRUSTED)\n"
	            "#   Internal network interface:\n"
	            "#     Interface(\"eth1\", network.LOCAL, profile.UNTRUSTED)\n"
	            "#   Trusted internal network interface:\n"
	            "#     Interface(\"eth2\", network.LOCAL, profile.TRUSTED)\n\n");


	for (device = devices; device != NULL; device = g_slist_next (device)) {
		name = (gchar *)device->data;

		setup = devices_get_setup (name);

		if (!setup)
			continue;

		switch (setup->type) {
		  case NETWORK_TYPE_INTERNET: type = type_internet; break;
		  default: type = type_local; break;
		}
		
		switch (setup->profile) {
		  case FIREWALL_PROFILE_UNTRUSTED: profile = profile_untrusted; break;
		  case FIREWALL_PROFILE_TRUSTED: profile = profile_trusted; break;
		  default: profile = profile_ignored; break;
		}


		/* Interface("eth0", network.INTERNET, profile.UNTRUSTED) */
		fprintf (f, "Interface(\"%s\", %s, %s)\n", name, type, profile);
	}

	fprintf (f, "\n");

	fprintf (f, "#### --(Internet Connection Sharing)--\n"
		    "# Manage DHCP server for local network clients\n"
		    "DHCP_SERVER=%s\n", test_bool (PREFS_FW_DHCP_ENABLE));

	fprintf (f, "# Forward server's DNS settings to clients in DHCP lease\n");
	fprintf (f, "DHCP_DYNAMIC_DNS=\"on\"\n");

	fprintf (f, "\n");

	fprintf (f, "#### --(Inbound Traffic)--\n"
		    "# Packet rejection method\n"
		    "#   DROP:   Ignore the packet\n"
		    "#   REJECT: Send back an error packet in response\n");
	if (preferences_get_bool (PREFS_FW_DENY_PACKETS))
		fprintf (f, "STOP_TARGET=\"DROP\"\n");
	else
		fprintf (f, "STOP_TARGET=\"REJECT\"\n");

	fprintf (f, "\n");

	fprintf (f, "#### --(Outbound Traffic)--\n"
	            "# Default Outbound Traffic Policy\n"
		    "#   permissive: everything not denied is allowed\n"
		    "#   restrictive everything not allowed is denied\n");
	if (preferences_get_bool (PREFS_FW_RESTRICTIVE_OUTBOUND_MODE))
		fprintf (f, "OUTBOUND_POLICY=\"restrictive\"\n");
	else
		fprintf (f, "OUTBOUND_POLICY=\"permissive\"\n");

	fprintf (f, "\n");

	fprintf (f, "#### --(Type of Service)--\n"
		    "# Enable ToS filtering\n"
		    "FILTER_TOS=%s\n", test_bool (PREFS_FW_FILTER_TOS));
	fprintf (f, "# Apply ToS to typical client tasks such as SSH and HTTP\n"
		    "TOS_CLIENT=%s\n", test_bool (PREFS_FW_TOS_CLIENT));
	fprintf (f, "# Apply ToS to typical server tasks such as SSH, HTTP, HTTPS and POP3\n"
		    "TOS_SERVER=%s\n", test_bool (PREFS_FW_TOS_SERVER));
	fprintf (f, "# Apply ToS to Remote X server connections\n"
		    "TOS_X=%s\n", test_bool (PREFS_FW_TOS_X));

	fprintf (f, "# ToS parameters\n"
		    "#   4:  Maximize Reliability\n"
		    "#   8:  Maximize-Throughput\n"
		    "#   16: Minimize-Delay\n");

	if (preferences_get_bool (PREFS_FW_TOS_OPT_TROUGHPUT))
		fprintf (f, "TOSOPT=\"8\"\n");
	else if (preferences_get_bool (PREFS_FW_TOS_OPT_RELIABILITY))
		fprintf (f, "TOSOPT=\"4\"\n");
	else if (preferences_get_bool (PREFS_FW_TOS_OPT_DELAY))
		fprintf (f, "TOSOPT=\"16\"\n");
	else
		fprintf (f, "TOSOPT=\"\"\n");

	fprintf (f, "\n");

	fprintf (f, "#### --(ICMP Filtering)--\n"
		    "# Enable ICMP filtering\n"
		    "FILTER_ICMP=%s\n", test_bool (PREFS_FW_FILTER_ICMP));
	fprintf (f, "# Allow Echo requests\n"
		    "ICMP_ECHO_REQUEST=%s\n", test_bool (PREFS_FW_ICMP_ECHO_REQUEST));
	fprintf (f, "# Allow Echo replies\n"
		    "ICMP_ECHO_REPLY=%s\n", test_bool (PREFS_FW_ICMP_ECHO_REPLY));
	fprintf (f, "# Allow Traceroute requests\n"
		    "ICMP_TRACEROUTE=%s\n", test_bool (PREFS_FW_ICMP_TRACEROUTE));
	fprintf (f, "# Allow MS Traceroute Requests\n"
		    "ICMP_MSTRACEROUTE=%s\n", test_bool (PREFS_FW_ICMP_MSTRACEROUTE));
	fprintf (f, "# Allow Unreachable Requests\n"
		    "ICMP_UNREACHABLE=%s\n", test_bool (PREFS_FW_ICMP_UNREACHABLE));
	fprintf (f, "# Allow Timestamping Requests\n"
		    "ICMP_TIMESTAMPING=%s\n", test_bool (PREFS_FW_ICMP_TIMESTAMPING));
	fprintf (f, "# Allow Address Masking Requests\n"
		    "ICMP_MASKING=%s\n", test_bool (PREFS_FW_ICMP_MASKING));
	fprintf (f, "# Allow Redirection Requests\n"
		    "ICMP_REDIRECTION=%s\n", test_bool (PREFS_FW_ICMP_REDIRECTION));
	fprintf (f, "# Allow Source Quench Requests\n"
		    "ICMP_SOURCE_QUENCHES=%s\n", test_bool (PREFS_FW_ICMP_SOURCE_QUENCHES));

	fprintf (f, "\n");

	fprintf (f, "#### --(Broadcast Traffic)--\n"
		    "# Block external broadcast traffic\n"
		    "BLOCK_EXTERNAL_BROADCAST=%s\n", test_bool (PREFS_FW_BLOCK_EXTERNAL_BROADCAST));
	fprintf (f, "# Block internal broadcast traffic\n"
		    "BLOCK_INTERNAL_BROADCAST=%s\n", test_bool (PREFS_FW_BLOCK_INTERNAL_BROADCAST));

	fprintf (f, "\n");

	fprintf (f, "#### --(Traffic Validation)--\n"
		    "# Block non-routable traffic on the public interfaces\n"
		    "BLOCK_NON_ROUTABLES=%s\n", test_bool (PREFS_FW_BLOCK_NON_ROUTABLES));

	fprintf (f, "\n");

	fprintf (f, "#### --(Logging)--\n"
		    "# System log level\n"
		    "LOG_LEVEL=");
	if (g_file_test ("/etc/debian_version", G_FILE_TEST_EXISTS))
		fprintf (f, "\"DEBUG\"\n");
	else
		fprintf (f, "\"INFO\"\n");

	fprintf (f, "\n");

	fclose (f);
}

static gboolean
file_exists (const gchar *path)
{
	return g_file_test (path, G_FILE_TEST_EXISTS);
}

/*
static gboolean
dhclient_is_running (void)
{
	gboolean exists;
	
	gchar *path = g_strconcat ("/var/run/dhclient-",
				preferences_get_string (PREFS_FW_EXT_IF),
				".pid", NULL);

	exists = file_exists (path);
	g_free (path);

	return exists;
}

static gboolean
dhcpcd_is_running (void)
{
	gboolean exists;
	gchar *path;
	
	if (file_exists ("/etc/slackware-version")) {
		path = g_strconcat ("/etc/dhcpc/dhcpcd-",
			 preferences_get_string (PREFS_FW_EXT_IF),
			 ".pid", NULL);
	} else {
		path = g_strconcat ("/var/run/dhcpcd-",
			 preferences_get_string (PREFS_FW_EXT_IF),
			 ".pid", NULL);
	}

	exists = file_exists (path);
	g_free (path);

	return exists;
}
*/

static void
append_hook_to_script (FILE *f)
{
	gchar buf[512];
	GList *list = NULL;
	GList *link;

	while (fgets (buf, 512, f) != NULL) {
		if (strstr (buf, FIRESTARTER_HOOK))
			return;
		else
			list = g_list_append (list, g_strdup (buf));
	}

	rewind (f);
	fprintf (f, FIRESTARTER_HOOK);

	link = list;
	while (link != NULL) {
		fprintf (f, link->data);
		g_free (link->data);
		link = link->next;
	}

	g_list_free (list);
}

static void
remove_hook (gchar *path)
{
	FILE *f;
	gchar buf[512];
	GList *list = NULL;
	GList *link = NULL;
	gint pos = 0;

	f = fopen (path, "r");

	if (f == NULL) {
		perror (g_strconcat ("Could not remove firestarter hook in ", path, NULL));
		return;
	}

	while (fgets (buf, 512, f) != NULL) {
		list = g_list_append (list, g_strdup (buf));
		if (strstr (buf, FIRESTARTER_HOOK))
			link = g_list_nth (list, pos);
			
		pos++;
	}

	fclose (f);

	if (link != NULL) {
		GList *newlist;
		
		newlist = g_list_remove_link (list, link);
		g_free (link->data);

		f = fopen (path, "w");

		if (f == NULL) {
			perror (g_strconcat ("Could not remove firestarter hook in ", path, NULL));
			return;
		}

		link = newlist;
		while (link != NULL) {
			fprintf (f, link->data);
			g_free (link->data);
			link = link->next;
		}

		g_list_free (newlist);
		fclose (f);
	}
}

static void
add_hook (gchar *path)
{
	FILE *f;

	printf ("Adding Firestarter startup hook to %s\n", path);

	if (file_exists (path)) {
		f = fopen (path, "r+");

		if (f == NULL) {
			perror ("Could not append firestarter hook");
			return;
		}

		append_hook_to_script (f);
		fclose (f);

	} else {
		f = fopen (path, "w");

		if (f == NULL) {
			perror ("Could not write firestarter hook");
			return;
		}

		fprintf (f, FIRESTARTER_HOOK);
		fclose (f);
	}
}

void
scriptwriter_write_ppp_hook (void)
{
	if (!file_exists ("/etc/ppp")) {
		printf ("No ppp detected on system. Not adding starting hook\n");
		return;
	}

	add_hook (PPP_HOOK_FILE);
	chmod (PPP_HOOK_FILE, 0755);
}

void
scriptwriter_remove_ppp_hook (void)
{
	if (!file_exists ("/etc/ppp/ip-up.local")) {
		return;
	}

	remove_hook (PPP_HOOK_FILE);
}

/*
void
scriptwriter_write_dhcp_hook (void)
{
*/
	/* Red Hat 8+, some Mandrake 9 configurations use dhclient */
/*	if (dhclient_is_running ()) {
		gchar *path = g_strdup ("/etc/dhclient-exit-hooks");

		add_hook (path);
		g_free (path);
*/
	/* Slackware uses DHCPCD, but it's path is different */
/*	} else if (dhcpcd_is_running () && file_exists ("/etc/slackware-version")) {
		gchar *path = g_strconcat ("/etc/dhcpc/dhcpcd-",
					   preferences_get_string (PREFS_FW_EXT_IF),
					   ".exe", NULL);

		add_hook (path);
		g_free (path);
*/
	/* Most other distributions use DHCPCD */
/*	} else if (dhcpcd_is_running ()) {
		gchar *path = g_strconcat ("/etc/dhcpcd/dhcpcd-",
					   preferences_get_string (PREFS_FW_EXT_IF),
					   ".exe", NULL);

		add_hook (path);
		g_free (path);
	}
}

void
scriptwriter_remove_dhcp_hook (void)
{
	gchar *path;
*/
	/* Red Hat, Fedora, SuSE, Mandrake dhclient */
/*	if (file_exists ("/etc/dhclient-exit-hooks")) {
		path = g_strdup ("/etc/dhclient-exit-hooks");

		remove_hook (path);
		g_free (path);
	}
*/
	/* Slackware DHCPD */
/*	path = g_strconcat ("/etc/dhcpc/dhcpcd-",
			   preferences_get_string (PREFS_FW_EXT_IF),
			   ".exe", NULL);
	if (file_exists (path)) {
		remove_hook (path);
	}
	g_free (path);
*/
	/* Old DHCPCD */
/*	path = g_strconcat ("/etc/dhcpcd/dhcpcd-",
			   preferences_get_string (PREFS_FW_EXT_IF),
			   ".exe", NULL);
	if (file_exists (path)) {
		remove_hook (path);
	}
	g_free (path);
}
*/

/* [ check_file ]
 * Check that file exists, if not, create
 */
static void
check_file (const gchar *path)
{
	FILE *file = NULL;

	if ((fopen (path, "r") == NULL) && (errno == ENOENT)) {
	        if ((file = fopen (path, "w")) != NULL) {
			chmod (path, 00660);
			fclose (file);
        	}
	}
}

/* [ create_rules_files ]
 * Create the empty modrules and user scripts, unless they already exist
 */
static void
create_rules_files (void)
{
	check_file (FIRESTARTER_USER_PRE_SCRIPT);
	check_file (FIRESTARTER_USER_POST_SCRIPT);
	check_file (FIRESTARTER_FILTER_HOSTS_SCRIPT);
	check_file (FIRESTARTER_FILTER_PORTS_SCRIPT);

	check_file (POLICY_IN_ALLOW_FROM);
	check_file (POLICY_IN_ALLOW_SERVICE);
	check_file (POLICY_IN_FORWARD);
	check_file (POLICY_OUT_DENY_TO);
	check_file (POLICY_OUT_DENY_FROM);
	check_file (POLICY_OUT_DENY_SERVICE);
	check_file (POLICY_OUT_ALLOW_TO);
	check_file (POLICY_OUT_ALLOW_FROM);
	check_file (POLICY_OUT_ALLOW_SERVICE);
}

/* [ scriptwriter_output_scripts ]
 * Creates all of the firestarter scripts
 */
void
scriptwriter_output_scripts (void)
{
	/* Creating the directories for scripts if they are missing */
	mkdir (FIRESTARTER_RULES_DIR "/firestarter", 00770);
	mkdir (POLICY_IN_DIR, 00770);
	mkdir (POLICY_OUT_DIR, 00770);

	/* Write the firewall configuration */
	scriptwriter_output_configuration ();

	/* Create all of the rule file stubs */
	create_rules_files ();

	if (preferences_get_bool (PREFS_FIRST_RUN))
		policyview_install_default_ruleset ();

	/* Start firewall on DCHP lease renewal */
/*	if (preferences_get_bool (PREFS_START_ON_DHCP))
		scriptwriter_write_dhcp_hook ();
	else
		scriptwriter_remove_dhcp_hook ();
*/

	/* Mark that we have complete a configuration */
	preferences_set_bool (PREFS_FIRST_RUN, FALSE);
}

/* Check that the scripts on the system and the scripts that could be
   generated by this version of the program match */
gboolean
scriptwriter_versions_match (void)
{
	FILE *f;
	gchar buf[512];
	gchar *version;
	gboolean current;

	if (!file_exists (FIRESTARTER_CONFIGURATION_SCRIPT))
		return FALSE;

	f = fopen (FIRESTARTER_CONFIGURATION_SCRIPT, "r");
	fgets (buf, 512, f);
	version = get_text_between (buf, "Firestarter ", " configuration");

	current = g_str_equal (version, VERSION);
	g_free (version);
	fclose (f);

	return current;
}
