/*---[ devices.h ]-----------------------------------------------------
 * Copyright (C) 2005 Tomas Junnonen (tomas@fs-security.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Functions for querying the network interfaces
 *--------------------------------------------------------------------*/

#ifndef _FIRESTARTER_DEVICES
#define _FIRESTARTER_DEVICES

#include <config.h>
#include <gnome.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <sys/ioctl.h>

#define DEVICE_FILE "/proc/net/dev"
#define DEVICE_HISTORY_LENGTH 5 /* Number of samples to use when averaging the traffic rate */
#define DEVICE_REFRESH_RATE 1 /* Time in seconds between samples */

typedef enum
{
	NETWORK_TYPE_INTERNET,
	NETWORK_TYPE_LOCAL,
} Network_type;

typedef enum
{
	FIREWALL_PROFILE_UNTRUSTED,
	FIREWALL_PROFILE_TRUSTED,
	FIREWALL_PROFILE_IGNORED,
} Firewall_profile;

typedef struct
{
	Network_type type;
	Firewall_profile profile;
	gboolean share;
	gboolean dhcp;
	gchar *dhcp_low_ip;
	gchar *dhcp_high_ip;
} Device_setup;

typedef struct
{
	gulong received;
	gulong sent;
	gulong previous_total;
	float  average;
	float *traffic_history;
	gint   history_index;
} Device_metrics;

typedef struct
{
	gchar *name;
	Device_setup *setup;
	Device_metrics *metrics;
} Device;

guint devices_count (void);
GSList *devices_get_names (void);
gchar *devices_get_pretty_name (gchar *interface, gboolean long_form);
gchar *devices_get_ip (gchar *itf);
gchar *devices_get_subnet (gchar *itf);
GtkTreeModel* devices_get_model (void);

Device_setup *devices_get_setup (gchar *device_name);
void devices_save_setup (GSList *list);
Device_metrics *devices_get_metrics (gchar *device_name);

void devices_refresh (void);

#endif
