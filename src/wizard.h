/*---[ wizard.h ]------------------------------------------------------
 * Copyright (C) 2000-2005 Tomas Junnonen (majix@sci.fi)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The firewall setup wizard header file
 *--------------------------------------------------------------------*/

#ifndef _FIRESTARTER_WIZARD
#define _FIRESTARTER_WIZARD

#include <config.h>
#include <gnome.h>

void run_wizard (void);

#endif
