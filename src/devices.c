/*---[ devices.c ]-----------------------------------------------------
 * Copyright (C) 2005 Tomas Junnonen (tomas@fs-security.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Functions for querying the network devices in the machine
 *--------------------------------------------------------------------*/

#include "devices.h"
#include "util.h"
#include "preferences.h"

/* Hastable of device structures, key is device string */
static GHashTable *devices = NULL;

static void
init_devices (void)
{
	devices = g_hash_table_new (g_str_hash, g_str_equal);
	devices_refresh ();
}

guint
devices_count (void)
{
	if (!devices)
		init_devices();

	return g_hash_table_size (devices);
}

static void
append_name (gchar *device_name, Device *device, GSList **names)
{
	*names = g_slist_append(*names, device_name);
}

GSList *
devices_get_names (void)
{
	GSList *names = NULL;

	if (!devices)
		init_devices();

	g_hash_table_foreach (devices, (GHFunc)append_name, &names);
	return names;
}

gchar *
devices_get_pretty_name (gchar *interface, gboolean long_form)
{
	gchar *name;

	if (g_str_has_prefix (interface, "eth"))
		if (long_form)
			name = g_strdup (_("Ethernet device"));
		else
			name = g_strdup (_("Ethernet"));
	else if (g_str_has_prefix (interface, "ppp"))
		if (long_form)
			name = g_strdup (_("Dialup device"));
		else
			name = g_strdup (_("Dialup"));
	else if (g_str_has_prefix (interface, "wlan"))
		if (long_form)
			name = g_strdup (_("Wireless device"));
		else
			name = g_strdup (_("Wireless"));
	else if (g_str_has_prefix (interface, "sit"))
		name = g_strdup (_("Virtual IPv6 device"));
	else if (g_str_has_prefix (interface, "tap"))
		name = g_strdup (_("VPN tunnel device"));
	else if (g_str_has_prefix (interface, "tun"))
		name = g_strdup (_("Routed IP Tunnel"));
	else
		if (long_form)
			name = g_strdup (_("Unknown device"));
		else
			name = g_strdup (_("Unknown"));
	
	if (long_form) {
		gchar *long_name;
		
		long_name = g_strconcat (name, " (", interface, ")", NULL);
		g_free (name);
		return long_name;
	} else
		return name;
	
}

GtkTreeModel*
devices_get_model (void)
{
	static GtkTreeModel *model = NULL;
	GtkTreeIter iter;
	GSList *devices, *device;
	gchar *name;


	if (model != NULL)
		return model;

	model = (GtkTreeModel *)gtk_list_store_new (2,
		G_TYPE_STRING, G_TYPE_STRING);

	devices = devices_get_names ();
	for (device = devices; device != NULL; device = g_slist_next (device)) {
		name = (gchar *)device->data;

		gtk_list_store_append (GTK_LIST_STORE (model), &iter);

		gtk_list_store_set (GTK_LIST_STORE (model), &iter,
		                    0, devices_get_pretty_name (name, TRUE),
	                            1, name,
		                    -1);
	}

	return model;
}

Device_setup *
devices_get_setup (gchar *device_name)
{
	Device *device = NULL;
	Device_setup *setup = NULL;

	if (!devices)
		init_devices();

	device = g_hash_table_lookup (devices, device_name);
	if (device) {
		if (device->setup != NULL)
			memcpy (setup, device->setup, sizeof (Device_setup));
		else {
			gchar *key;

			setup = g_new0 (Device_setup, 1);
			if (preferences_in_list (PREFS_FW_EXT_INTERFACES, device_name))
				setup->type = NETWORK_TYPE_INTERNET;
			else if (preferences_in_list (PREFS_FW_INT_INTERFACES, device_name))
				setup->type = NETWORK_TYPE_LOCAL;

			if (preferences_in_list (PREFS_FW_TRUSTED_INTERFACES, device_name))
				setup->profile = FIREWALL_PROFILE_TRUSTED;
			else if (preferences_in_list (PREFS_FW_IGNORED_INTERFACES, device_name))
				setup->profile = FIREWALL_PROFILE_IGNORED;
			else
				setup->profile = FIREWALL_PROFILE_UNTRUSTED;
			
			key = g_strconcat (PREFS_FW_DEVICES, device_name, "_share", NULL);
			setup->share = preferences_get_bool (key);
			g_free (key);

			key = g_strconcat (PREFS_FW_DEVICES, device_name, "_dhcp", NULL);
			setup->dhcp = preferences_get_bool (key);
			g_free (key);

			key = g_strconcat (PREFS_FW_DEVICES, device_name, "_dhcp_low_ip", NULL);
			setup->dhcp_low_ip = preferences_get_string (key);
			g_free (key);

			/* Provide some default lower ip range limit based on the interface's IP,
			   in order to avoid IP space clashes with multiple internal interfaces */
			if (!setup->dhcp_low_ip) {
				gchar *ip, *low, *last_octet;
				
				ip = devices_get_ip (device_name);
				last_octet = rindex (ip, '.');
				low = g_strndup (ip, last_octet-ip);

				setup->dhcp_low_ip = g_strconcat (low, ".100", NULL);
				g_free (low);
				g_free (ip);
			}

			key = g_strconcat (PREFS_FW_DEVICES, device_name, "_dhcp_high_ip", NULL);
			setup->dhcp_high_ip = preferences_get_string (key);
			g_free (key);

			if (!setup->dhcp_high_ip) {
				gchar *ip, *high, *last_octet;
				
				ip = devices_get_ip (device_name);
				last_octet = rindex (ip, '.');
				high = g_strndup (ip, last_octet-ip);

				setup->dhcp_high_ip = g_strconcat (high, ".254", NULL);
				g_free (high);
				g_free (ip);
			}


		}

		return setup;
	} else
		return NULL;
}

Device_metrics *
devices_get_metrics (gchar *device_name)
{
	Device *device = NULL;

	if (!devices)
		init_devices();

	device = g_hash_table_lookup (devices, device_name);
	if (device)
		return device->metrics;
	else
		return NULL;
}


/* [ devices_get_ip ]
 * Get the IP address in use by a device
 */
gchar*
devices_get_ip (gchar *itf) {
	int fd;	
	struct ifreq ifreq;
	struct sockaddr_in *sin;
	gchar *ip;

	fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
	strcpy(ifreq.ifr_name, itf);
	ioctl(fd, SIOCGIFADDR, &ifreq);
	sin = (struct sockaddr_in *)&ifreq.ifr_broadaddr;

	ip = g_strdup(inet_ntoa(sin->sin_addr));

	close (fd);
	return ip;
}

/* [ devices_get_subnet ]
 * Get the subnet-mask used by a device
 */
gchar*
devices_get_subnet (gchar *itf) {
	int fd;	
	struct ifreq ifreq;
	struct sockaddr_in *sin;
	gchar *subnet;

	fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
	strcpy(ifreq.ifr_name, itf);
	ioctl(fd, SIOCGIFNETMASK, &ifreq);
	sin = (struct sockaddr_in *)&ifreq.ifr_broadaddr;

	subnet = g_strdup(inet_ntoa(sin->sin_addr));
	
	return subnet;
}

/* [ refresh_traffic_average ]
 * Recalculate the traffic averages for a network device
 */
static void
refresh_traffic_average (Device_metrics *metrics)
{
	float current = metrics->received + metrics->sent;
	float last = metrics->previous_total;
	float difference = current - last;
	float average = 0.0;
	gint seconds = DEVICE_REFRESH_RATE;
	gint i;

	if (last == 0) {
		metrics->previous_total = current;
		return;
	}

	difference /= seconds;

	/* Update the history */
	metrics->traffic_history[metrics->history_index++] = difference;
	if (metrics->history_index == DEVICE_HISTORY_LENGTH)
		metrics->history_index = 0;

	/* Calculate the average */
	for (i = 0; i < DEVICE_HISTORY_LENGTH; i++) {
		average += metrics->traffic_history[i];
	}
	average /= DEVICE_HISTORY_LENGTH;

	/* Store current total for next refresh */
	metrics->previous_total = current;

	metrics->average = average;
}

/* [ refresh_devices ]
 * Refresh all available data for all the network devices in the machine
 */
void
devices_refresh (void)
{
	gint line_num = 0;
	gchar *line;
	static GIOChannel *in = NULL;
	GError *error = NULL;
	gchar *str;
	gchar *name;
	Device *device;
	Device_metrics *metrics = NULL;

	if (!devices)
		init_devices();

	if (in == NULL) {
		in = g_io_channel_new_file (DEVICE_FILE, "r", &error);

		if (in == NULL) {
			printf ("Error reading %s: %s\n", DEVICE_FILE, error->message);
			return;
		}
	}

	while (TRUE) {
		unsigned long int values[16];
		
		g_io_channel_read_line (in, &line, NULL, NULL, &error);

		if (line == NULL) // EOF reached
			break;
		line_num++;
		if (line_num <= 2) { // Skip dev headers
			g_free (line);
			continue;
		}

		name = g_strstrip(g_strndup (line, 6)); /* Extract interface name */

		/* Interface blacklist */
		if (g_str_equal (name, "lo")) {
			g_free (line);
			continue;
		}


		metrics = devices_get_metrics (name);
		if (metrics == NULL) { /* Initialize new device structure */
			gint i;
			device = g_new0 (Device, 1);
			metrics = g_new0 (Device_metrics, 1);

			/* Initialize device traffic history, for counting average activity */
			metrics->traffic_history = (float*)malloc (DEVICE_HISTORY_LENGTH * sizeof (float));
			metrics->history_index = 0;
			for (i = 0; i < DEVICE_HISTORY_LENGTH; i++)
				metrics->traffic_history[i] = 0.0;
			metrics->previous_total = 0;

			device->setup = NULL;
			device->metrics = metrics;

			g_hash_table_insert (devices, g_strdup (name), device);
		}
		refresh_traffic_average (metrics); /* Recalculate average traffic */
		g_free (name);
			
		str = line+7; // Continue parsing after interface

		/* values array:
		recieve:  0 bytes 1 packets 2 errs 3 drop 4 fifo 5 frame 6 compressed 7 multicast
		transmit: 8 bytes 9 packets 10 errs 11 drop 12 fifo 13 colls 14 carrier 15 compressed */
		sscanf(str,
			"%lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu",
			&values[0], &values[1], &values[2], &values[3], &values[4],
			&values[5], &values[6], &values[7], &values[8], &values[9],
			&values[10], &values[11], &values[12], &values[13], &values[14],
			&values[15]);

		metrics->received = values[0];
		metrics->sent = values[8];

		g_free (line);
	}

	g_io_channel_seek_position (in, 0, G_SEEK_SET, &error); // Rewind
}
